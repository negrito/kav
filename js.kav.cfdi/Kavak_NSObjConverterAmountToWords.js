//=====================================================================================================================================================================
//
// Script File	: NSObjConverterAmountToWords.js
//
// Script Type  : Library/Object
//
// Description 	: Converter from amount to words
//
// Author		: Gerardo A. Luna - Netsoft
//
// Date			: 24-07-2010
//
// Notes        : Used in "NSOConvertAmountToWords.js" file
//
//=====================================================================================================================================================================


function NSObjConverterAmountToWords()
{
	//this.language = language;

	this.currencyLanguage = new Array();

	initCurrencyLanguage(this.currencyLanguage);

	this.toWords = function(number, language, currency)
	{
		var words = "";

		var objNumber = new objDecimalNumber(number);

		switch(language)
		{
			case 'spanish':
				words = AmountToSpanishWords(objNumber.integerPart.toString());
				break;
			case 'english':
				words = AmountToEnglishWords(objNumber.integerPart.toString());
				break;
			case 'french':
				words = AmountToFrenchWords(objNumber.integerPart.toString());
				break;
			case 'portuguese':
				words = AmountToPortugueseWords(objNumber.integerPart.toString());
				break;
		}
		var objCurrLang = getCurrencyLanguage(this.currencyLanguage, language, currency);
		if (!objCurrLang) {
			return '';
		}
		var pluralorsingle = objNumber.integerPart == 1 ? objCurrLang.single : objCurrLang.plural;

		objNumber.decimalPart = /\.[0-9]{1,2}/.exec(number);
		objNumber.decimalPart = objNumber.decimalPart ? objNumber.decimalPart[0]: '.0';
		objNumber.decimalPart = objNumber.decimalPart.substring(1, objNumber.decimalPart.length);
		objNumber.decimalPart += '';
		objNumber.decimalPart += objNumber.decimalPart.length == 1 ? '0' : '';

		if(currency == 'HNL')
			words = words + ' ' + pluralorsingle + ' ' + objNumber.decimalPart + "/100 " + objCurrLang.symbol;
		else{
			var Simbolo = objCurrLang.symbol||'';
			if(Simbolo == ''){objCurrLang.symbol = 'MXN'}
			words = words + ' con ' + objNumber.decimalPart + "/100 "+  objCurrLang.symbol;
		    words = words.toUpperCase();
		}
		return words;
	}

}

function objDecimalNumber(number)
{

	this.number = number;
	this.integerPart = 0;
	this.decimalPart = 0;

	var arrnum = number.toString().split(".");

	if (arrnum.length == 1){
	
		this.integerPart = parseInt(arrnum[0], 10);
		this.decimalPart = 0;
	} else {

		if (!isNaN(arrnum[0]) == true){
			this.integerPart = arrnum[0] * 1;
		}
		else{
			this.integerPart = 0;
		}
		if (!isNaN(arrnum[1]) == true){
			this.decimalPart = arrnum[1] * 1;
		}
		else{
			this.decimalPart = 0;
		}
	}
}

function objCurrencyLanguage(language, currency, symbol, plural, single)
{
	this.language = language;
	this.currency = currency;
	this.symbol = symbol == null || symbol == "" ? currency : symbol;
	this.plural = plural;
	this.single = single;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

//--> Initialize Currency-Language Matrix
function initCurrencyLanguage(currencyLanguage)
{

	currencyLanguage[0] = new Array();
	currencyLanguage[0][0] = new objCurrencyLanguage("spanish", "MXN", "PESOS, MXN.", "pesos", "peso");
	currencyLanguage[0][1] = new objCurrencyLanguage("spanish", "USD", null, "dolares", "dolar");
	currencyLanguage[0][2] = new objCurrencyLanguage("spanish", "EUR", null, "euros", "euro");
	currencyLanguage[0][3] = new objCurrencyLanguage("spanish", "HNL", null, "Lempiras", "Lempira");
	currencyLanguage[0][4] = new objCurrencyLanguage("spanish", "CNY", null, "RMB", "RMB");
	currencyLanguage[0][5] = new objCurrencyLanguage("spanish", "ARS", null, "peso AR", "pesos AR");
	currencyLanguage[0][6] = new objCurrencyLanguage("spanish", "CAD", null, "dolar canadiense", "dolares canadienses");
	currencyLanguage[0][7] = new objCurrencyLanguage("spanish", "GBP", null, "British pound", "British pound");
	currencyLanguage[0][8] = new objCurrencyLanguage("spanish", "COP", null, "pesos", "Pesos Colombianos");
	currencyLanguage[0][9] = new objCurrencyLanguage("spanish", "CLP", null, "pesos", "Pesos Chilenos");
	currencyLanguage[0][10] = new objCurrencyLanguage("spanish", "GBP", null, "Libras Esterlinas ", "Libra Esterlina ");
	currencyLanguage[0][11] = new objCurrencyLanguage("spanish", "CRC", null, "Colones", "Colón");

	currencyLanguage[1] = new Array();
	currencyLanguage[1][0] = new objCurrencyLanguage("english", "MXN", null, "pesos", "peso");
	currencyLanguage[1][1] = new objCurrencyLanguage("english", "USD", null, "dlls", "dll");
	currencyLanguage[1][2] = new objCurrencyLanguage("english", "HNL", null, "Lempiras", "Lempira");
	currencyLanguage[1][3] = new objCurrencyLanguage("english", "EUR", null, "euros", "euro");
	currencyLanguage[1][4] = new objCurrencyLanguage("english", "GBP", null, "Libras Esterlinas ", "Libra Esterlina ");

	currencyLanguage[2] = new Array();
	currencyLanguage[2][0] = new objCurrencyLanguage("french", "MXN", null, "pesos", "peso");
	currencyLanguage[2][1] = new objCurrencyLanguage("french", "USD", null, "dlls", "dll");
	currencyLanguage[2][2] = new objCurrencyLanguage("french", "EUR", null, "euros", "euro");
	currencyLanguage[2][3] = new objCurrencyLanguage("french", "GBP", null, "Libras Esterlinas ", "Libra Esterlina ");
	currencyLanguage[2][4] = new objCurrencyLanguage("french", "HNL", null, "Lempiras", "Lempira");

	currencyLanguage[3] = new Array();
	currencyLanguage[3][0] = new objCurrencyLanguage("portuguese", "MXN", null, "pesos", "peso");
	currencyLanguage[3][1] = new objCurrencyLanguage("portuguese", "USD", null, "dlls", "dll");
	currencyLanguage[3][2] = new objCurrencyLanguage("portuguese", "EUR", null, "euros", "euro");
	currencyLanguage[3][3] = new objCurrencyLanguage("portuguese", "GBP", null, "Libras Esterlinas ", "Libra Esterlina ");
	currencyLanguage[3][4] = new objCurrencyLanguage("portuguese", "HNL", null, "Lempiras", "Lempira");

}

//--> Search Currency-Language Item
function getCurrencyLanguage(matrix, language, currency)
{
	var objRet = null;

	for(var i = 0; matrix != null && i < matrix.length; i++)
	{
		for(var j = 0; j < matrix[i].length; j++)
		{
			if(matrix[i][j].language == language && matrix[i][j].currency == currency)
			{
				objRet = matrix[i][j];
				break;
			}
		}
	}

	return objRet;

}


//-------------------------------------------------------------
//-------------- Converter from amounts to words --------------
//-------------------------------------------------------------
function AmountToEnglishWords(s){
//English words for numbers
	var a,b,c,j,orlen,result='';
	if (s=='0') {return ('zero');}
		orlen=s.length;
	if ((s.length % 3)>0)
		s=' '+s;
	if ((s.length % 3)>0)
		s=' '+s;
	for (var i = 0; i < s.length; i=i+3) {
		j=s.length-i-1;
		a=s.substring(j, j+1);
		b=s.substring(j-1, j);
		c=s.substring(j-2, j-1);
		if (a!=' '){
			if ((i==3)&(c+b+a!='000') ) {result='thousand '+result;}
			else if ((i==6)&(c+b+a!='000') ) {result='million '+result;}
			else if ((i==9)&(c+b+a!='000') ) {result='billion '+result;}
			else if ((i==12)&(c+b+a!='000') ) {result='trillion '+result;}
			else if ((i==15)&(c+b+a!='000') ) {result='quadrillion '+result;}
			else if ((i==18)&(c+b+a!='000') ) {result='quintillion '+result;}
			else if ((i==21)&(c+b+a!='000') ) {result='sextillion '+result;}
			else if ((i==24)&(c+b+a!='000') ) {result='septillion '+result;}
			else if ((i==27)&(c+b+a!='000') ) {result='octillion '+result;}
			else if ((i==30)&(c+b+a!='000') ) {result='nonillion '+result;}
			else if ((i==33)&(c+b+a!='000') ) {result='decillion '+result;}
			else if ((i==36)&(c+b+a!='000') ) {result='undecillion '+result;}
			else if ((i==39)&(c+b+a!='000') ) {result='duodecillion '+result;}
			else if ((i==42)&(c+b+a!='000') ) {result='tredecillion '+result;}
			else if ((i==45)&(c+b+a!='000') ) {result='quattuordecillion '+result;}
			else if ((i==48)&(c+b+a!='000') ) {result='quindecillion '+result;}
			else if ((i==51)&(c+b+a!='000') ) {result='sexdecillion '+result;}
			else if ((i==54)&(c+b+a!='000') ) {result='septendecillion '+result;}
			else if ((i==57)&(c+b+a!='000') ) {result='octodecillion '+result;}
			else if (i==60) {result='novemdecillion '+result;}
		}
		if ((b!='1') | (b==' ')){
			if (a==1){result='one '+result;}
			else if (a==2){result='two '+result;}
			else if (a==3){result='three '+result;}
			else if (a==4){result='four '+result;}
			else if (a==5){result='five '+result;}
			else if (a==6){result='six '+result;}
			else if (a==7){result='seven '+result;}
			else if (a==8){result='eight '+result;}
			else if (a==9){result='nine '+result;}
		}
		if ((b!=' ')&(b!='0')){
			if (b=='1'){
				if (a==0){result='ten '+result;}
				else if (a==1){result='eleven '+result;}
				else if (a==2){result='twelve '+result;}
				else if (a==3){result='thirteen '+result;}
				else if (a==4){result='fourteen '+result;}
				else if (a==5){result='fifteen '+result;}
				else if (a==6){result='sixteen '+result;}
				else if (a==7){result='seventeen '+result;}
				else if (a==8){result='eighteen '+result;}
				else if (a==9){result='nineteen '+result;}
			}
			else{
				if (b==2){result='twenty '+result;}
				else if (b==3){result='thirty '+result;}
				else if (b==4){result='fourty '+result;}
				else if (b==5){result='fifty '+result;}
				else if (b==6){result='sixty '+result;}
				else if (b==7){result='seventy '+result;}
				else if (b==8){result='eighty '+result;}
				else if (b==9){result='ninety '+result;}
			}
		}
		if ((c!=' ')&(c!='0')){
			if (c==1){result='one hundred '+result;}
			else if (c==2){result='two hundred '+result;}
			else if (c==3){result='three hundred '+result;}
			else if (c==4){result='four hundred '+result;}
			else if (c==5){result='five hundred '+result;}
			else if (c==6){result='six hundred '+result;}
			else if (c==7){result='seven hundred '+result;}
			else if (c==8){result='eight hundred '+result;}
			else if (c==9){result='nine hundred '+result;}
		}
	}
	result=Trim(result);
	result=FixSpaces(result);
	return (result);
}
function AmountToSpanishWords(s){
//Spanish words for numbers
	var a,b,c,j,orlen,result='';
	if (s=='0') {return ('cero');}
		orlen=s.length;
	if ((s.length % 3)>0)
		s=' '+s;
	if ((s.length % 3)>0)
		s=' '+s;
	for (var i = 0; i < s.length; i=i+3) {
		j=s.length-i-1;
		a=s.substring(j, j+1);
		b=s.substring(j-1, j);
		c=s.substring(j-2, j-1);
		if (a!=' '){
			if ((i==3)&(c+b+a!='000') ) {result='mil '+result;}
			else if (((i==6)&(c+b+a!='000') ) &(orlen==7)&(a=='1')) {result='millon '+result;}
			else if ((i==6)&(c+b+a!='000') ) {result='millones '+result;}
			else if ((i==9)&(c+b+a!='000') ) {result='mil millones'+result;}
			else if (((i==12)&(c+b+a!='000') ) & (orlen==13)&(a=='1')) {result='billon '+result;}
			else if ((i==12)&(c+b+a!='000') ) {result='billones '+result;}
			else if ((i==15)&(c+b+a!='000') ) {result='mil billones'+result;}
			else if (((i==18)&(c+b+a!='000') )& (orlen==19)&(a=='1'))  {result='trillon '+result;}
			else if ((i==18)&(c+b+a!='000') ) {result='trillones '+result;}
			else if ((i==21)&(c+b+a!='000') ) {result='mil trillones '+result;}
			else if (((i==24)&(c+b+a!='000') )& (orlen==25)&(a=='1')) {result='quadrillon '+result;}
			else if ((i==24)&(c+b+a!='000') ) {result='quadrillones '+result;}
			else if ((i==27)&(c+b+a!='000') ) {result='mil quadrillones '+result;}
			else if (((i==30)&(c+b+a!='000') )& (orlen==31)&(a=='1')) {result='quintillon '+result;}
			else if ((i==30)&(c+b+a!='000') ) {result='quintillones '+result;}
			else if ((i==33)&(c+b+a!='000') ) {result='mil quintillones '+result;}
			else if (((i==36)&(c+b+a!='000') )& (orlen==37)&(a=='1')) {result='sextillon '+result;}
			else if ((i==36)&(c+b+a!='000') ) {result='sextillones '+result;}
			else if ((i==39)&(c+b+a!='000') ) {result='mil sextillones '+result;}
			else if (((i==42)&(c+b+a!='000') )& (orlen==43)&(a=='1')) {result='septillon '+result;}
			else if ((i==42)&(c+b+a!='000') ) {result='septillones '+result;}
			else if ((i==45)&(c+b+a!='000') ) {result='milseptillones '+result;}
			else if (((i==48)&(c+b+a!='000') )& (orlen==49)&(a=='1')) {result='octillon '+result;}
			else if ((i==48)&(c+b+a!='000') ) {result='octillones '+result;}
			else if ((i==51)&(c+b+a!='000') ) {result='mil octillones '+result;}
			else if (((i==54)&(c+b+a!='000') )& (orlen==55)&(a=='1')) {result='nonillon '+result;}
			else if ((i==57)&(c+b+a!='000') ) {result='nonillones '+result;}
			else if (i==60) {result='mil nonillones '+result;}
		}
		if ((b!=1 & b!=2) | (b==' ')){
			if (a==1){result='un '+result;}
			else if (a==2){result='dos '+result;}
			else if (a==3){result='tres '+result;}
			else if (a==4){result='cuatro '+result;}
			else if (a==5){result='cinco '+result;}
			else if (a==6){result='seis '+result;}
			else if (a==7){result='siete '+result;}
			else if (a==8){result='ocho '+result;}
			else if (a==9){result='nueve '+result;}
		}
		if ((b!=' ')&(b!='0')){
			if ((b==1) | (b==2)){
				if (b+a==10){result='diez '+result;}
				else if (b+a==11){result='once '+result;}
				else if (b+a==12){result='doce '+result;}
				else if (b+a==13){result='trece '+result;}
				else if (b+a==14){result='catorce '+result;}
				else if (b+a==15){result='quince '+result;}
				else if (b+a==16){result='dieciseis '+result;}
				else if (b+a==17){result='diecisiete '+result;}
				else if (b+a==18){result='dieciocho '+result;}
				else if (b+a==19){result='diecinueve '+result;}
				else if (b+a==20){result='veinte '+result;}
				else if (b+a==21){result='veintiun '+result;}
				else if (b+a==22){result='veintidos '+result;}
				else if (b+a==23){result='veintitres '+result;}
				else if (b+a==24){result='veinticuatro '+result;}
				else if (b+a==25){result='veinticinco '+result;}
				else if (b+a==26){result='veintiseis '+result;}
				else if (b+a==27){result='veintisiete '+result;}
				else if (b+a==28){result='veintiocho '+result;}
				else if (b+a==29){result='veintinueve '+result;}
			}
			else{
				var temp=''
				if (a!=0){temp='y ';}
				if (b==3){result='treinta '+temp+result;}
				else if (b==4){result='cuarenta '+temp+result;}
				else if (b==5){result='cincuenta '+temp+result;}
				else if (b==6){result='sesenta '+temp+result;}
				else if (b==7){result='setenta '+temp+result;}
				else if (b==8){result='ochenta '+temp+result;}
				else if (b==9){result='noventa '+temp+result;}

				}
		}
		if ((c!=' ')&(c!='0')){
			if ((a=='0') & (b=='0')){
				if (c==1){result='cien '+result;}
				else if (c==2){result='doscientos '+result;}
				else if (c==3){result='trescientos '+result;}
				else if (c==4){result='cuatrocientos '+result;}
				else if (c==5){result='quinientos '+result;}
				else if (c==6){result='seiscientos '+result;}
				else if (c==7){result='setecientos '+result;}
				else if (c==8){result='ochocientos '+result;}
				else if (c==9){result='novecientos '+result;}
			}
			else{
				if (c==1){result='ciento '+result;}
				else if (c==2){result='doscientos '+result;}
				else if (c==3){result='trescientos '+result;}
				else if (c==4){result='cuatrocientos '+result;}
				else if (c==5){result='quinientos '+result;}
				else if (c==6){result='seiscientos '+result;}
				else if (c==7){result='setecientos '+result;}
				else if (c==8){result='ochocientos '+result;}
				else if (c==9){result='novecientos '+result;}
			}
		}
	}
	result=FixSpaces(result);
	result=Trim(result);
	if (result.substring(0, 7)=='un mil ') result=result.substring(3,result.length);
	if (result.substring(result.length-3, result.length)==' un') result=result+'o';
	if (result=='un ') result='uno';
	if (result.substring(result.length-2, result.length)=='y ')
		result=result.substring(0,result.length-2);
if (InStr(result, 'millones')!=RInStr(result, 'millones'))
	{var z=InStr(result, 'millones')
	result=result.substring(0,z-1)+result.substring(z+7,result.length);}
if (InStr(result, 'billones')!=RInStr(result, 'billones'))
	{var z=InStr(result, 'billones')
	result=result.substring(0,z-1)+result.substring(z+7,result.length);}
if (InStr(result, 'trillones')!=RInStr(result, 'trillones'))
	{var z=InStr(result, 'trillones')
	result=result.substring(0,z-1)+result.substring(z+8,result.length);}
if (InStr(result, 'quadrillones')!=RInStr(result, 'quadrillones'))
	{var z=InStr(result, 'quadrillones')
	result=result.substring(0,z-1)+result.substring(z+11,result.length);}
if (InStr(result, 'quintillones')!=RInStr(result, 'quintillones'))
	{var z=InStr(result, 'quintillones')
	result=result.substring(0,z-1)+result.substring(z+11,result.length);}
if (InStr(result, 'sextillones')!=RInStr(result, 'sextillones'))
	{var z=InStr(result, 'sextillones')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'septillones')!=RInStr(result, 'septillones'))
	{var z=InStr(result, 'septillones')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'octillones')!=RInStr(result, 'octillones'))
	{var z=InStr(result, 'octillones')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
if (InStr(result, 'nonillones')!=RInStr(result, 'nonillones'))
	{var z=InStr(result, 'nonillones')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
	result=FixSpaces(result);
	return (result);
}
function AmountToFrenchWords (s){
//French words for numbers
	var a,b,c,j,orlen,result='';
	if (s=='0') {return ('zero');}
		orlen=s.length;
	if ((s.length % 3)>0)
		s=' '+s;
	if ((s.length % 3)>0)
		s=' '+s;
	for (var i = 0; i < s.length; i=i+3) {
		j=s.length-i-1;
		a=s.substring(j, j+1);
		b=s.substring(j-1, j);
		c=s.substring(j-2, j-1);
		if (a!=' '){
			if ((i==3)&(c+b+a!='000') ) {result='mille '+result;}
			else if (((i==6)&(c+b+a!='000') ) &(orlen==7)&(a=='1')) {result='million '+result;}
			else if ((i==6)&(c+b+a!='000') ) {result='millions '+result;}
			else if ((i==9)&(c+b+a!='000') ) {result='mille millions'+result;}
			else if (((i==12)&(c+b+a!='000') ) & (orlen==13)&(a=='1')) {result='billion '+result;}
			else if ((i==12)&(c+b+a!='000') ) {result='billions '+result;}
			else if ((i==15)&(c+b+a!='000') ) {result='mille billions'+result;}
			else if (((i==18)&(c+b+a!='000') )& (orlen==19)&(a=='1'))  {result='trillion '+result;}
			else if ((i==18)&(c+b+a!='000') ) {result='trillions '+result;}
			else if ((i==21)&(c+b+a!='000') ) {result='mille trillions '+result;}
			else if (((i==24)&(c+b+a!='000') )& (orlen==25)&(a=='1')) {result='quadrillion '+result;}
			else if ((i==24)&(c+b+a!='000') ) {result='quadrillions '+result;}
			else if ((i==27)&(c+b+a!='000') ) {result='mille quadrillions '+result;}
			else if (((i==30)&(c+b+a!='000') )& (orlen==31)&(a=='1')) {result='quintillion '+result;}
			else if ((i==30)&(c+b+a!='000') ) {result='quintillions '+result;}
			else if ((i==33)&(c+b+a!='000') ) {result='mille quintillions '+result;}
			else if (((i==36)&(c+b+a!='000') )& (orlen==37)&(a=='1')) {result='sextillion '+result;}
			else if ((i==36)&(c+b+a!='000') ) {result='sextillions '+result;}
			else if ((i==39)&(c+b+a!='000') ) {result='mille sextillions '+result;}
			else if (((i==42)&(c+b+a!='000') )& (orlen==43)&(a=='1')) {result='septillion '+result;}
			else if ((i==42)&(c+b+a!='000') ) {result='septillions '+result;}
			else if ((i==45)&(c+b+a!='000') ) {result='milseptillions '+result;}
			else if (((i==48)&(c+b+a!='000') )& (orlen==49)&(a=='1')) {result='octillion '+result;}
			else if ((i==48)&(c+b+a!='000') ) {result='octillions '+result;}
			else if ((i==51)&(c+b+a!='000') ) {result='mille octillions '+result;}
			else if (((i==54)&(c+b+a!='000') )& (orlen==55)&(a=='1')) {result='nonillion '+result;}
			else if ((i==57)&(c+b+a!='000') ) {result='nonillions '+result;}
			else if (i==60) {result='mille nonillions '+result;}
		}
		if (((b!=1)&(b!=7)) | (b==' ')){
			if (a==1){result='un '+result;}
			else if (a==2){result='deux '+result;}
			else if (a==3){result='trois '+result;}
			else if (a==4){result='quatre '+result;}
			else if (a==5){result='cinq '+result;}
			else if (a==6){result='six '+result;}
			else if (a==7){result='sept '+result;}
			else if (a==8){result='huit '+result;}
			else if (a==9){result='neuf '+result;}
		}
		if ((b!=' ')&(b!='0')){
			if ((b==1) |(b==7) ){
				if (b+a==10){result='dix '+result;}
				else if (b+a==11){result='onze '+result;}
				else if (b+a==12){result='douze '+result;}
				else if (b+a==13){result='treize '+result;}
				else if (b+a==14){result='quatorze '+result;}
				else if (b+a==15){result='quinze '+result;}
				else if (b+a==16){result='seize '+result;}
				else if (b+a==17){result='dix-sept '+result;}
				else if (b+a==18){result='dix-huit '+result;}
				else if (b+a==19){result='dix-neuf '+result;}
				else if (b+a==70){result='soixante-dix '+result;}
				else if (b+a==71){result='soixante-onze '+result;}
				else if (b+a==72){result='soixante-douze '+result;}
				else if (b+a==73){result='soixante-treize '+result;}
				else if (b+a==74){result='soixante-quatorze '+result;}
				else if (b+a==75){result='soixante-quinze '+result;}
				else if (b+a==76){result='soixante-seize '+result;}
				else if (b+a==77){result='soixante-dix-sept '+result;}
				else if (b+a==78){result='soixante-dix-huit '+result;}
				else if (b+a==79){result='soixante-dix-neuf '+result;}
				}
			else{
				var temp=''
				if (a==1){temp='et ';}
				if (a>1){temp='-';}
				if (b==2){result='vingt '+temp+result;}
				else if (b==3){result='trente '+temp+result;}
				else if (b==4){result='quarante '+temp+result;}
				else if (b==5){result='cinquante '+temp+result;}
				else if (b==6){result='soixante '+temp+result;}
				else if (b==7){result='soixante-dix '+temp+result;}
				else if (b==8){result='quatre-vingts '+temp+result;}
				else if (b==9){result='quatre-vingt-dix '+temp+result;}
				}
		}
		if ((c!=' ')&(c!='0')){
				if (c==1){result='cent '+result;}
				else if (c==2){result='deux cents '+result;}
				else if (c==3){result='trois cents '+result;}
				else if (c==4){result='quatre cents '+result;}
				else if (c==5){result='cinq cents '+result;}
				else if (c==6){result='six cents '+result;}
				else if (c==7){result='sept cents '+result;}
				else if (c==8){result='huit cents '+result;}
				else if (c==9){result='neuf cents '+result;}
		}
	}
	result=FixSpaces(result);
	result=Trim(result);
	if (result.substring(0, 9)=='un mille ') result=result.substring(3,result.length);
	if (result.substring(result.length-3, result.length)=='et ')
		result=result.substring(0,result.length-3);
	if (result.substring(result.length-2, result.length)==' -')
		result=result.substring(0,result.length-2);
if (InStr(result, 'millions')!=RInStr(result, 'millions'))
	{var z=InStr(result, 'millions')
	result=result.substring(0,z-1)+result.substring(z+7,result.length);}
if (InStr(result, 'billions')!=RInStr(result, 'billions'))
	{var z=InStr(result, 'billions')
	result=result.substring(0,z-1)+result.substring(z+7,result.length);}
if (InStr(result, 'trillions')!=RInStr(result, 'trillions'))
	{var z=InStr(result, 'trillions')
	result=result.substring(0,z-1)+result.substring(z+8,result.length);}
if (InStr(result, 'quadrillions')!=RInStr(result, 'quadrillions'))
	{var z=InStr(result, 'quadrillions')
	result=result.substring(0,z-1)+result.substring(z+11,result.length);}
if (InStr(result, 'quintillions')!=RInStr(result, 'quintillions'))
	{var z=InStr(result, 'quintillions')
	result=result.substring(0,z-1)+result.substring(z+11,result.length);}
if (InStr(result, 'sextillions')!=RInStr(result, 'sextillions'))
	{var z=InStr(result, 'sextillions')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'septillions')!=RInStr(result, 'septillions'))
	{var z=InStr(result, 'septillions')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'octillions')!=RInStr(result, 'octillions'))
	{var z=InStr(result, 'octillions')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
if (InStr(result, 'nonillions')!=RInStr(result, 'nonillions'))
	{var z=InStr(result, 'nonillions')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
	result=FixSpaces(result);
while (InStr(result, ' -')>0){
	var z=InStr(result, ' -');
	result=result.substring(0,z-1)+result.substring(z,result.length);
	}
return (result);
}
function AmountToPortugueseWords(s){
//Portuguese words for numbers
	var a,b,c,j,orlen,result='';
	if (s=='0') {return ('zero');}
		orlen=s.length;
	if ((s.length % 3)>0)
		s=' '+s;
	if ((s.length % 3)>0)
		s=' '+s;
	for (var i = 0; i < s.length; i=i+3) {
		j=s.length-i-1;
		a=s.substring(j, j+1);
		b=s.substring(j-1, j);
		c=s.substring(j-2, j-1);
		if (a!=' '){
			var tb=s.substring(i, i+1), tc=s.substring(s.length-2,s.length-1), td=s.substring(s.length-1,s.length);
			if ((i==3)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil '+result;}
			else if (((i==6)&(c+b+a!='000') ) &(orlen==7)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='milh�o '+result;}
			else if ((i==6)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='milh�es '+result;}
			else if ((i==9)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil milh�es'+result;}
			else if (((i==12)&(c+b+a!='000') ) & (orlen==13)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='bili�o '+result;}
			else if ((i==12)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='bili�es '+result;}
			else if ((i==15)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil bili�es'+result;}
			else if (((i==18)&(c+b+a!='000') )& (orlen==19)&(a=='1'))  {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='trili�o '+result;}
			else if ((i==18)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='trili�es '+result;}
			else if ((i==21)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil trili�es '+result;}
			else if (((i==24)&(c+b+a!='000') )& (orlen==25)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='quadrili�o '+result;}
			else if ((i==24)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='quadrili�es '+result;}
			else if ((i==27)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil quadrili�es '+result;}
			else if (((i==30)&(c+b+a!='000') )& (orlen==31)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='quintili�o '+result;}
			else if ((i==30)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='quintili�es '+result;}
			else if ((i==33)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil quintili�es '+result;}
			else if (((i==36)&(c+b+a!='000') )& (orlen==37)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='sextili�o '+result;}
			else if ((i==36)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='sextili�es '+result;}
			else if ((i==39)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil sextili�es '+result;}
			else if (((i==42)&(c+b+a!='000') )& (orlen==43)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='septili�o '+result;}
			else if ((i==42)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='septili�es '+result;}
			else if ((i==45)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='milseptili�es '+result;}
			else if (((i==48)&(c+b+a!='000') )& (orlen==49)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='octili�o '+result;}
			else if ((i==48)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='octili�es '+result;}
			else if ((i==51)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil octili�es '+result;}
			else if (((i==54)&(c+b+a!='000') )& (orlen==55)&(a=='1')) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='nonili�o '+result;}
			else if ((i==57)&(c+b+a!='000') ) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='nonili�es '+result;}
			else if (i==60) {if((tb=='0')&(tc!='0')|(tb=='0')&(td!='0')) result=' e '+result;result='mil nonili�es '+result;}
		}
		if ((b!=1) | (b==' ')){
			if (a==1){result='um '+result;}
			else if (a==2){result='dois '+result;}
			else if (a==3){result='tr�s '+result;}
			else if (a==4){result='quatro '+result;}
			else if (a==5){result='cinco '+result;}
			else if (a==6){result='seis '+result;}
			else if (a==7){result='sete '+result;}
			else if (a==8){result='oito '+result;}
			else if (a==9){result='nove '+result;}
		}
		if ((b!=' ')&(b!='0')){
			if ((b==1)){
				if (b+a==10){result='dez '+result;}
				else if (b+a==11){result='onze '+result;}
				else if (b+a==12){result='doze '+result;}
				else if (b+a==13){result='treze '+result;}
				else if (b+a==14){result='catorze '+result;}
				else if (b+a==15){result='quinze '+result;}
				else if (b+a==16){result='dezasseis '+result;}
				else if (b+a==17){result='dezassete '+result;}
				else if (b+a==18){result='dezoito '+result;}
				else if (b+a==19){result='dezanove '+result;}
				else if (b+a==20){result='vinte '+result;}
			}
			else{
				var temp=''
				if (a!=0){temp='e ';}
				if (b==2){result='vinte '+temp+result;}
				else if (b==3){result='trinta '+temp+result;}
				else if (b==4){result='quarenta '+temp+result;}
				else if (b==5){result='cinquenta '+temp+result;}
				else if (b==6){result='sessenta '+temp+result;}
				else if (b==7){result='setenta '+temp+result;}
				else if (b==8){result='oitenta '+temp+result;}
				else if (b==9){result='noventa '+temp+result;}

				}
		}
		if ((c!=' ')&(c!='0')){
			if ((a=='0') & (b=='0')){
				if (c==1){result='e cem '+result;}
				else if (c==2){result='e duzentos '+result;}
				else if (c==3){result='e trezentos '+result;}
				else if (c==4){result='e quatrocentos '+result;}
				else if (c==5){result='e quinhentos '+result;}
				else if (c==6){result='e seiscentos '+result;}
				else if (c==7){result='e setecentos '+result;}
				else if (c==8){result='e oitocentos '+result;}
				else if (c==9){result='e novecentos '+result;}
			}
			else{
				if (c==1){result='cento e '+result;}
				else if (c==2){result='duzentos e '+result;}
				else if (c==3){result='trezentos e '+result;}
				else if (c==4){result='quatrocentos e '+result;}
				else if (c==5){result='quinhentos e '+result;}
				else if (c==6){result='seiscentos e '+result;}
				else if (c==7){result='setecentos e '+result;}
				else if (c==8){result='oitocentos e '+result;}
				else if (c==9){result='novecentos e '+result;}
			}
		}
	}
	result=FixSpaces(result);
	result=Trim(result);
	if (result.substring(0, 7)=='um mil ') result=result.substring(3,result.length);
	if (result.substring(result.length-2, result.length)=='e ')
		result=result.substring(0,result.length-2);
if (InStr(result, 'milh�es')!=RInStr(result, 'milh�es'))
	{var z=InStr(result, 'milh�es')
	result=result.substring(0,z-1)+result.substring(z+6,result.length);}
if (InStr(result, 'bili�es')!=RInStr(result, 'bili�es'))
	{var z=InStr(result, 'bili�es')
	result=result.substring(0,z-1)+result.substring(z+6,result.length);}
if (InStr(result, 'trili�es')!=RInStr(result, 'trili�es'))
	{var z=InStr(result, 'trili�es')
	result=result.substring(0,z-1)+result.substring(z+7,result.length);}
if (InStr(result, 'quadrili�es')!=RInStr(result, 'quadrili�es'))
	{var z=InStr(result, 'quadrili�es')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'quintili�es')!=RInStr(result, 'quintili�es'))
	{var z=InStr(result, 'quintili�es')
	result=result.substring(0,z-1)+result.substring(z+10,result.length);}
if (InStr(result, 'sextili�es')!=RInStr(result, 'sextili�es'))
	{var z=InStr(result, 'sextili�es')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
if (InStr(result, 'septili�es')!=RInStr(result, 'septili�es'))
	{var z=InStr(result, 'septili�es')
	result=result.substring(0,z-1)+result.substring(z+9,result.length);}
if (InStr(result, 'octili�es')!=RInStr(result, 'octili�es'))
	{var z=InStr(result, 'octili�es')
	result=result.substring(0,z-1)+result.substring(z+8,result.length);}
if (InStr(result, 'nonili�es')!=RInStr(result, 'nonili�es'))
	{var z=InStr(result, 'nonili�es')
	result=result.substring(0,z-1)+result.substring(z+8,result.length);}
	result=FixSpaces(result);
	return (result);
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------
// ------------------- String functions ------------------
//--------------------------------------------------------
function InStr(n, s1, s2) {
	// Devuelve la posici�n de la primera ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=InStr.arguments.length;

	if(numargs<3)
		return n.indexOf(s1)+1;
	else
		return s1.indexOf(s2, n)+1;
}
function RInStr(n, s1, s2){
	// Devuelve la posici�n de la �ltima ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=RInStr.arguments.length;

	if(numargs<3)
		return n.lastIndexOf(s1)+1;
	else
		return s1.lastIndexOf(s2, n)+1;
}
function LTrim(s){
	// Devuelve una cadena sin los espacios del principio
	var i=0;
	var j=0;

	// Busca el primer caracter <> de un espacio
	for(i=0; i<=s.length-1; i++)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(j, s.length);
}
function RTrim(s){
	// Quita los espacios en blanco del final de la cadena
	var j=0;

	// Busca el �ltimo caracter <> de un espacio
	for(var i=s.length-1; i>-1; i--)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(0, j+1);
}
function Trim(s){
	// Quita los espacios del principio y del final
	return LTrim(RTrim(s));
}
function FixSpaces(s){
//returns a string with no double spaces inside
	var t='';
	for(var i=0; i<s.length; i++){
		if (i>0){
			if (!((s.substring(i-1,i)==' ')&(s.substring(i,i+1)==' ')))
				t=t+s.substring(i,i+1);
		}else
			t=t+s.substring(i,i+1);
	}
	return t;
}
function REPLICATE(n, c){
	//returns n times char c
	var t='';
	for(var i=1; i<=n; i++)
		t=t+c;
	return t;
}
