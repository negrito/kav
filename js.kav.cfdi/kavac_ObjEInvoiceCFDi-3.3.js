//===========================================================================================
// Company        : NetSoft, www.netsoft.com.mx
// Name of Script : McGeever_ObjEInvoiceCFDi.js
// Author         : Ivan Gonzalez - Netsoft - Mexico
// Date			  : 22-02-2012
// Base			  : DEV
// Type           : Library
// Sub-Type       : Business Process
// Categories     : Business logic
// Description    : Generate eInvoice layout 
// NetSuite Ver.  : (Edition: International) Release 2011.2
// Dependences    : File Name
//                   --------------------
//                   NSOUtils.js
//                   NSOCountriesStates.js
//===========================================================================================


var idretIVA = '1-2';
var idretISR = '1-3';

var versioncomplemento     = nlapiGetContext().getSetting('SCRIPT','custscript_version_complemento');
var id_clave_prodse_group  = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_claveprod_item')||'';
var id_UnidadLine_group    = nlapiGetContext().getSetting('SCRIPT','custscript_id_unidad_medida_itemgroup')||'';
var id_frac_aran_group     = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_fraccaran_item')||'';
var id_claveunidad_group   = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_unidadmed_item')||'';
var id_campo_rfc           = nlapiGetContext().getSetting('SCRIPT','custscript_id_custbody_rfc')||'custbody_rfc';
var cambioValoresNC        = nlapiGetContext().getSetting('SCRIPT','custscript_cfdi_cambio_valores_cfdi')||'F';
var id_mail_customer       = nlapiGetContext().getSetting('SCRIPT','custscript_id_field_mail_customer')||'custbody_email_cliente';
var dif_centavos           = nlapiGetContext().getSetting('SCRIPT','custscript_dif_centavos')||0.02;
var campo_anticipo         = nlapiGetContext().getSetting('SCRIPT','custscript_id_custbody_anticipo')||'custbody_cfdi_anticipo';
 

//nlapiLogExecution('DEBUG','metrica: ' + metrica);
function NSObjEInvoiceCFDi() 
{
    
    this.invoice       = null;
    this.customer      = null;
    this.params        = null;
	this.setupcfdi     = null;
    this.salesorder    = null;
	this.body          = "";	
	this.subtotal      = 0;
	this.subtotalbruto = 0;
	this.tasades       = null;
	this.tasatax       = null;
	this.tasaretencion = null;	
	this.descuento     = 0;	
	this.newtax16      = 0;	
	this.newRetax16    = 0;	
	    
    this.build = function(id, setupid){
		
		setupid  = setupid == null || setupid == "" ? 1 : setupid;
		var invoice     = nsoGetTranRecord(id); 
        var custid      = invoice.getFieldValue("entity");
        var customer    = nlapiLoadRecord("customer", custid);
        var params      = nlapiLoadRecord("customrecord_cfdisetup", setupid);
		var setupcfdi   = nlapiLoadRecord("customrecord_setup_cfdi", 1);	
        var customform  = invoice.getFieldValue("customform");
        var comext      = invoice.getFieldValue('custbody_cfdi_comercio_exterior');
		var idcustomformCE      = nlapiGetContext().getSetting('SCRIPT','custscript_id_customform_cext');
		  
		  //----> Asignación de variables globales
		this.id          = id;
        this.invoice     = invoice;
        this.customer    = customer;
        this.params      = params;        
		this.setupid     = setupid;

		var ComercioExterior = 'F';
		if(comext == 'T' && anticipo == 'F'){
			ComercioExterior = 'T';
		}
		var anticipo               = invoice.getFieldValue(campo_anticipo) ||'F'; 
		//----> Generación de XML
		var bodytext = '';
		bodytext += '<fx:FactDocMX xmlns:fx="http://www.fact.com.mx/schema/fx" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.fact.com.mx/schema/fx http://www.mysuitemex.com/fact/schema/fx_2010_f.xsd">';
		

        bodytext += '<fx:Version>' + setupcfdi.getFieldValue('custrecord_cfdi_version')  + '</fx:Version>';
      	bodytext += nlGetCFDi01(invoice, customer, params, setupcfdi, ComercioExterior);
		bodytext += nlGetCFDiH4(invoice, customer, params, setupcfdi, ComercioExterior);
        bodytext += nlGetCFDi02(invoice, customer, params, setupcfdi, ComercioExterior, anticipo);
		bodytext += Totales(invoice, customer, params, setupcfdi, ComercioExterior, anticipo);
		
        if(ComercioExterior == 'T'){
       		bodytext += Complementos(invoice, customer, params, setupcfdi);
		}
        bodytext += ComprobanteEx(invoice, customer, params, setupcfdi, ComercioExterior, anticipo);
		
        this.body = bodytext;
		bodytext  = ChartCode(bodytext)
		return bodytext;
    }
}




//-----------------------------------Inicia trama 01------------------------------------------------------------------------------------
 
function Complementos(invoice, customer, params, setupcfdi){
	var retval                    = '';
	var version                   = params.getFieldValue('custrecord_cfdi_comext_version');
	var TipoOperacion             = params.getFieldValue('custrecord_cfdi_comext_tipooperacion');
	var ClaveDePedimento          = params.getFieldValue('custrecord_cfdi_comext_claveped')||'0';
	var CertificadoOrigen         = invoice.getFieldValue('custbody_cfdi_ce_cert_origen')||'';
	var NumCertificadoOrigen      = invoice.getFieldValue('custbody_cfdi_ce_num_certorigen');
	var NumeroExportadorConfiable = params.getFieldValue('custrecord_cfdi_comext_numexportador');
	var Subdivision               = invoice.getFieldValue('custbody_cfdi_ce_subdivision')||'0';
	var Observaciones             = invoice.getFieldValue('custbody_cfdi_ce_observaciones')||'';
	var TipoCambioUSD             = Math.abs(parseFloat((invoice.getFieldValue('exchangerate')))).toFixed(6);
	var sUBTotalUSD               = invoice.getFieldValue('subtotal');
	var Total                     = invoice.getFieldValue('total');
	var incotermlist              = invoice.getFieldValue('custbody_cfdi_incoterm');
	var idmedidacolumn            = nlapiGetContext().getSetting('SCRIPT','custscript_id_custcol_med');
	var MotivoTraslado            = invoice.getFieldText('custbody_cfdi_mottraslado');
	if (MotivoTraslado != '' && MotivoTraslado != null){
		var MotivoArray = MotivoTraslado.split('-');
		MotivoTraslado  = MotivoArray[0];
	}
	var billaddresslist           = invoice.getFieldValue("billaddresslist");
	var shipaddresslist           = isNull(invoice.getFieldValue("shipaddresslist"));
	var moneda                    = invoice.getFieldValue('currencysymbol');
	var recordtype                = invoice.getRecordType();
	var idmedidacolumn            = nlapiGetContext().getSetting('SCRIPT','custscript_id_custcol_med');
	if (idmedidacolumn == null || idmedidacolumn == ''){idmedidacolumn = 'units'}
	var cartaporte                = invoice.getFieldValue('custbody_cfdi_carta_porte')||'F';
	if (cartaporte == 'T' && recordtype == 'invoice'){
		idmedidacolumn = 'custcol_unidadmedidiace';
		Total = '0.01';
	}
	if (idmedidacolumn == null || idmedidacolumn == ''){idmedidacolumn = 'units'}
	

	var incoterm = 'CFR';
	if (incotermlist != null && incotermlist != ''){
		var arrayInc = new Array();
		arrayInc = incotermlist.split('-');
		if(arrayInc.length > 1){incoterm = arrayInc[0];}
	}
	var GetAgrupacionPais = '';
	for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){
		if (customer.getLineItemValue("addressbook", "id", i) == billaddresslist){
			var paisCE            = nsoGetCountryNameComExt(customer.getLineItemValue('addressbook', 'country', i));
			//nlapiLogExecution('DEBUG','paisCE: ' + paisCE);
			var GetAgrupacionPais = Catalogopaises(paisCE);
			//nlapiLogExecution('DEBUG','GetAgrupacionPais: ' + GetAgrupacionPais);
			if (GetAgrupacionPais == false){
				NumeroExportadorConfiable = '';
			}
		}
	}
	if (CertificadoOrigen == 0){
		NumCertificadoOrigen = '';
	}
	if (cartaporte != 'T'){
		MotivoTraslado = '';
		
	}
	var moneda          = invoice.getFieldValue('currencysymbol');
	if (moneda != 'MXN' && invoice.getFieldValue('exchangerate') == 1.00){
		var tipodecambio    = parseFloat(nlapiExchangeRate(moneda, 'MXN'));	
	}
	else{
		var tipodecambio    = parseFloat(invoice.getFieldValue('exchangerate'));
	}
	var date = new Date();
	date     = nlapiAddDays(date, -1);
	date     = nlapiDateToString(date)
	var rateexchange = nlapiExchangeRate('USD', 'MXN', date); 
	TipoCambioUSD = nsoParseFloatOrZero(rateexchange);
	
	Total = nsoParseFloatOrZero((Total*tipodecambio)/(TipoCambioUSD)).toFixed(2)
	//Complemento para incorporar la información en el caso de Exportación de Mercancías en definitiva.
	retval += '<fx:Complementos>';
	retval += '<fx:ComercioExterior11';
	if (MotivoTraslado != '' && MotivoTraslado != null && TipoOperacion != 'A'){
		retval += ' Version="'+ version + '" MotivoTraslado="' + MotivoTraslado + '" TipoOperacion="' + TipoOperacion + '"';
		
	}else{
		retval += ' Version="'+ version + '" TipoOperacion="' + TipoOperacion + '"';
	}
	if (TipoOperacion != 'A'){
		if(ClaveDePedimento != null && ClaveDePedimento != ''){
			retval += ' ClaveDePedimento="' + ClaveDePedimento + '"';
		}
		if(CertificadoOrigen != null && CertificadoOrigen != ''){
			retval += ' CertificadoOrigen="' + CertificadoOrigen + '"';
		}
		if(NumCertificadoOrigen != null && NumCertificadoOrigen != '' && CertificadoOrigen != 0){
			retval += ' NumCertificadoOrigen="' + NumCertificadoOrigen + '"';
		}
		if(NumeroExportadorConfiable != null && NumeroExportadorConfiable != ''){
			retval += ' NumeroExportadorConfiable="' + NumeroExportadorConfiable + '"';
		}
		if(incoterm != null && incoterm != ''){
			retval += ' Incoterm="' + incoterm + '"';
		}
		if(Subdivision != null && Subdivision != ''){
			retval += ' Subdivision="' + Subdivision + '"';
		}
		if(Observaciones != null && Observaciones != ''){
			retval += ' Observaciones="' + Observaciones + '"';
		}
		if(TipoCambioUSD != null && TipoCambioUSD != ''){
			retval += ' TipoCambioUSD="' + TipoCambioUSD + '"';
		}
		if(Total != null && Total != ''){
			retval += ' TotalUSD="' + Total + '"';
		}
	}//if (TipoOperacion != 'A'){' + Total + '"';
	
	retval +='>';

	var lengthrfc = '';
	var recRFC = invoice.getFieldValue('custbody_rfc');
	if(recRFC){
		var lengthrfc = recRFC.length;
	}

	var CURP =  invoice.getFieldValue('custentity_curp');
	if (CURP){
		retval += '<Emisor Curp="' + CURP  + '">';
	}


	else{
		var calle       = (params.getFieldValue('custrecord_cfdi_calleemisor'));	
		var numext      = isNull(params.getFieldValue('custrecord_cfdi_numexterioremisor'));
		var numint      = isNull(params.getFieldValue('custrecord_cfdi_numinterioremisor'));
		var localidadCE = isNull(params.getFieldValue('custrecord_cfdi_comext_localida'));
		var coloniaCE   = isNull(params.getFieldValue('custrecord_cfdi_comext_colonia'));
		var municipioCE = isNull(params.getFieldValue('custrecord_cfdi_comext_municipio'));
		var EstadoCE    = isNull(params.getFieldText('custrecord_cfdi_comext_estado'));
		var paisCE      = isNull(params.getFieldValue('custrecord_cfdi_comext_pais'));
		var CPCE        = isNull(params.getFieldValue('custrecord_cfdi_lugexpedicion_ce'))
		
		retval += '<fx:Emisor>';	
		retval += '<fx:Domicilio';
		retval += ' Calle="'+   calle  +'"';
		if (numext){
			retval += ' NumeroExterior="'+   numext  +'"';
		}
		if (numint){
			retval += ' NumeroInterior="'+   numint  +'"';
		}
		if (coloniaCE){
			retval += ' Colonia="'+   coloniaCE  +'"';
		}
		if (localidadCE){
			retval += ' Localidad="'+   localidadCE  +'"';
		}
		if (municipioCE){
			retval += ' Municipio="'+   municipioCE  +'"';
		}
		retval += ' Estado="'+   EstadoCE  +'"';
		retval += ' Pais="'  +    paisCE    +'"';
		retval += ' CodigoPostal="'+  CPCE +'"';
		retval +='/>';
		retval += '</fx:Emisor>';
	}




	var NumRegIdTrib = customer.getFieldValue('custentity_numregidtrib');
	//Nodo requerido para capturar los datos complementarios del receptor del CFDI.
	

	if (MotivoTraslado == '05' && recordtype == 'invoice'){
		retval += '<fx:Propietario NumRegIdTrib="' + NumRegIdTrib + '"' + ' ResidenciaFiscal="' + paisCE +'"';
		retval +='/>';
	}

	retval += '<fx:Receptor>'
	

	
	var billaddresslist = invoice.getFieldValue("billaddresslist");
	for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){
		if (customer.getLineItemValue("addressbook", "id", i) == billaddresslist){
			var codigopais    = (customer.getLineItemValue('addressbook', 'country', i));
			var pais          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
			var callereceptor = (customer.getLineItemValue('addressbook', 'addr1', i));
			var colonia       = (customer.getLineItemValue('addressbook', 'addr2', i));
			var Municipio     = (customer.getLineItemValue('addressbook', 'city', i));
			var Estado        = (customer.getLineItemValue('addressbook', 'state', i));
			var CP            = (customer.getLineItemValue('addressbook', 'zip', i));		
			customer.selectLineItem('addressbook', i);
			var subrecord     = customer.viewCurrentLineItemSubrecord('addressbook','addressbookaddress');
			var CPanexo       = subrecord.getFieldValue('custrecord_cfdi_anexo_cp');
			var paisCE        = nsoGetCountryNameComExt(customer.getLineItemValue('addressbook', 'country', i));	
			var numext        = subrecord.getFieldValue('custrecord_cfdi_numext')||'';
			var numint        = subrecord.getFieldValue('custrecord_cfdi_numint')||'';
			var localidad     = subrecord.getFieldValue('custrecord_cfdi_localidad')||'';
			if (codigopais == 'MEX' && ComercioExterior == 'T')	{
				var colonia    = subrecord.getFieldValue('custrecord_colonia_ce');
				var Estado     = subrecord.getFieldValue('custrecord_estado_ce');
				var Municipio  = subrecord.getFieldValue('custrecord_localidad_ce');
				var localidad  = subrecord.getFieldValue('custrecord_municipio_ce');
			}

						
			retval += '<fx:Domicilio';
			if (callereceptor){
				callereceptor = callereceptor.replace(/\./g, "");
				callereceptor = callereceptor.replace(/\//g, "");
				retval += ' Calle="'+   callereceptor  +'"';
			}
			
			if (numext){
				numext = numext.replace(/\./g, "");
				numext = numext.replace(/\//g, "");
				retval += ' NumeroExterior="'+   numext  +'"';
			}
			if (numint){
				numint = numint.replace(/\./g, "");
				numint = numint.replace(/\//g, "");
				retval += ' NumeroInterior="'+   numint  +'"';
			}
			if (colonia){
				colonia = colonia.replace(/\./g, "");
				colonia = colonia.replace(/\//g, "");
				retval += ' Colonia="'+   colonia  +'"';
			}
			if (localidad){
				retval += ' Localidad="'+   localidad  +'"';
			}
			if (Municipio){
				retval += ' Municipio="'+   Municipio  +'"';
			}
			if (Estado){
				retval += ' Estado="'+   Estado  +'"';
			}
			retval += ' Pais="'+    paisCE    +'"';
			if (CPanexo != null && CPanexo != ''){
				retval += ' CodigoPostal="'+  CP + '-' + CPanexo  +'"';
			}
			else{
				retval += ' CodigoPostal="'+  CP +'"';
			}
			retval +='/>';
		}
	}
	
	retval += '</fx:Receptor>';


	var nombreenvio = isNull(customer.getFieldValue('companyname'))||'';

	if(nombreenvio == ''){
		nombreenvio = nombreenvio.replace(/\./g, "");
		nombreenvio = nombreenvio.replace(/\//g, "");
	}



	//destinatario
	


	var shipaddresslist = invoice.getFieldValue("shipaddresslist")||'';
	for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){
		if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist){
			var nombreenvio   = customer.getLineItemValue('addressbook', 'addressee', i)||'';
			var codigopais    = customer.getLineItemValue('addressbook', 'country', i);
			var pais          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
			var callereceptor = (customer.getLineItemValue('addressbook', 'addr1', i));
			var colonia       = (customer.getLineItemValue('addressbook', 'addr2', i));
			var Municipio     = (customer.getLineItemValue('addressbook', 'city', i));
			var Estado        = (customer.getLineItemValue('addressbook', 'state', i));
			var CP            = (customer.getLineItemValue('addressbook', 'zip', i));		
			customer.selectLineItem('addressbook', i);
			var subrecord     = customer.viewCurrentLineItemSubrecord('addressbook','addressbookaddress');
			var NumRegIdTribD = subrecord.getFieldValue('custrecord_cfdi_numregidtrib')||NumRegIdTrib;
			var CPanexo       = subrecord.getFieldValue('custrecord_cfdi_anexo_cp');
			var paisCE        = nsoGetCountryNameComExt(customer.getLineItemValue('addressbook', 'country', i));	
			var numext        = subrecord.getFieldValue('custrecord_cfdi_numext');
			var numint        = subrecord.getFieldValue('custrecord_cfdi_numint');
			if (codigopais == 'MEX' && ComercioExterior == 'T')	{
				var colonia    = subrecord.getFieldValue('custrecord_colonia_ce');
				var Estado     = subrecord.getFieldValue('custrecord_estado_ce');
				var Municipio  = subrecord.getFieldValue('custrecord_localidad_ce');
				var localidad  = subrecord.getFieldValue('custrecord_municipio_ce');
			}

			retval += '<fx:Destinatario ';
			if(NumRegIdTribD != null && NumRegIdTribD != ''){
				retval += ' NumRegIdTrib="' + NumRegIdTribD + '"';
			}
			if(nombreenvio != null && nombreenvio != ''){
				retval += ' Nombre="'+   nombreenvio  +'"';
			}
			retval +='>';			
			retval += '<fx:Domicilio';
			if (callereceptor){
				callereceptor = callereceptor.replace(/\./g, "");
				callereceptor = callereceptor.replace(/\//g, "");
				retval += ' Calle="'+   callereceptor  +'"';
			}
			if (numext){
				numext = numext.replace(/\./g, "");
				numext = numext.replace(/\//g, "");
				retval += ' NumeroExterior="'+   numext  +'"';
			}
			if (numint){
				numint = numint.replace(/\./g, "");
				numint = numint.replace(/\//g, "");
				retval += ' NumeroInterior="'+   numint  +'"';
			}
			if (colonia){
				colonia = colonia.replace(/\./g, "");
				colonia = colonia.replace(/\//g, "");
				retval += ' Colonia="'+   colonia  +'"';
			}
			if (localidad){
				retval += ' Localidad="'+   localidad  +'"';
			}
			if (Municipio){
				retval += ' Municipio="'+   Municipio  +'"';
			}
			if (Estado){
				retval += ' Estado="'+   Estado  +'"';
			}
			retval += ' Pais="'+    paisCE    +'"';
			if (CPanexo != null && CPanexo != ''){
				retval += ' CodigoPostal="'+  CP + '-' + CPanexo  +'"';
			}
			else{
				retval += ' CodigoPostal="'+  CP +'"';
			}
			retval +='/>';
			retval += '</fx:Destinatario>';
			break;
		}
	}
	
	//Nodo opcional para capturar la información de la declaración de las mercancías exportadas.
	
	
	var wdiscountitem   = invoice.getFieldText('discountitem');
	var wglobaldiscount = Math.abs(parseFloat(invoice.getFieldValue('discounttotal')));
	var wglobalsubtotal = Math.abs(parseFloat(invoice.getFieldValue('subtotal'))); 
	var gift            = Math.abs(parseFloat(invoice.getFieldValue('giftcertapplied')));
	wglobaldiscount     = nsoParseFloatOrZero(wglobaldiscount) + nsoParseFloatOrZero(gift/1.16);
    var wdiscountrate   = (wglobaldiscount*100/(wglobalsubtotal));
	wdiscountrate       = isNaN(wdiscountrate) ? 0 : parseFloat(wdiscountrate);
	if (wdiscountrate != null && wdiscountrate != '')
	{wdiscountrate = wdiscountrate.toFixed(2);}
	
	var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cfdi_clave_unidadmedida'));
		columns.push(new nlobjSearchColumn('custrecord_cfdi_unidadmedida'));
	var searchresultMed  = nlapiSearchRecord('customrecord_cfdi_medida_comext', null, null, columns);
	

	var retvalmercancias          = '';
	var printitems                = '';
	var bandera                   = false;
	var amountgroup               = '';
	var discountingroup           = 0;
	var ratelinealdiscountingroup = '';
	var ClaveProdServgroup        = '';
	var ClaveMedida_group         = '';
	var FraccAran_group           = '';
	for (var j=1; j <= invoice.getLineItemCount('item'); j++){
		var witemid   = invoice.getLineItemValue('item', 'item', j);
		var xxtype    = invoice.getLineItemValue('item', 'itemtype', j);
		var line      = parseFloat(invoice.getLineItemValue('item', 'line', j));
		var ClaveProdServgroup  = '';
		var UnidadMedida_group  = '';
		var witemtype = xxtype;
		if (xxtype == 'Group'){
			var record = nlapiLoadRecord('itemgroup', witemid);
			printitems = record.getFieldValue('printitems');
			line = line+1;
			if (printitems != 'T'){

				if (id_clave_prodse_group){
					var ClaveProdServgroup  = record.getFieldValue(id_clave_prodse_group);
				}
				if (id_claveunidad_group){
					var ClaveMedida_group   = record.getFieldValue(id_claveunidad_group);
				}
				if (id_frac_aran_group){
					var FraccAran_group     = record.getFieldValue(id_frac_aran_group);
				}
			}
			
		}
		if (xxtype == 'Group' && printitems == 'T'){
			ratelinealdiscountingroup =  Math.abs(parseFloat(isNull(RateDiscountGroup(j, invoice))));
		}
		if (xxtype == 'EndGroup' && printitems == 'T'){
			ratelinealdiscountingroup =  '';
		}
		
		if (xxtype == 'Group' && printitems == 'T'){	
			j=j+1;		
		}		
		


		if (witemtype == 'InvtPart' ||witemtype == 'Assembly' ||witemtype == 'Kit' ||witemtype == 'Group' || witemtype == 'Markup' ||witemtype == 'NonInvtPart' ||witemtype == 'OthCharge' ||witemtype == 'Payment' ||witemtype == 'Service' ){
			
			var line      = parseFloat(invoice.getLineItemValue('item', 'line', j));
			var NoIdentificacion    = invoice.getLineItemValue('item', 'item', j);
			if (NoIdentificacion != null && NoIdentificacion != ''){
				if(NoIdentificacion.length > 30){
				   NoIdentificacion = NoIdentificacion.substr(0,30);
				}
			}
			var discount            = 0; 
			var FraccionArancelaria = invoice.getLineItemValue('item', 'custcol_cfdi_frac_arancelaria', j);
			var CantidadAduana      = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'quantity', j));
			var ValorUnitarioAduana = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'rate', j)).toFixed(2);
			var ValorDolares        = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j)).toFixed(2);
			var taxamount           = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'tax1amt', j));
			var taxrate             = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'taxrate1', j));
			var UnidadAduana        = invoice.getLineItemValue('item', 'custcol_cfdi_clave_unidad' , j);
			
			var amounttaxgroup      = 0;
			var discountmonto       = nsoParseFloatOrZero(wdiscountrate/100);
			if (witemtype == 'Group' && printitems == 'F'){
				
				for (var i = j+1; i <= invoice.getLineItemCount('item'); i++)
				{
					var wtype = invoice.getLineItemValue('item', 'itemtype', i);
					if (wtype == 'EndGroup'){
						amountgroup =  parseFloat(invoice.getLineItemValue('item', 'amount', i));
						break;
					}
					if  (wtype != 'EndGroup'){
						taxamount += nsoParseFloatOrZero(invoice.getLineItemValue('item', 'tax1amt', i))
					}
				}
				ValorUnitarioAduana = parseFloat(amountgroup/CantidadAduana);
				discountingroup = parseFloat(invoice.getLineItemValue('item', 'amount', i+1));
				if (discountingroup < 0){
					ValorUnitarioAduana = parseFloat(ValorUnitarioAduana+discountingroup);
				}
				else
				{discountingroup = 0;}
					CantidadAduana = 1;
					UnidadAduana        = ClaveMedida_group   
					FraccionArancelaria = FraccAran_group;
			}// end of if (witemtype == 'itemgroup' && printitems == 'F')


			discount                = DiscountForItem(line, invoice);
			if (discount < 0){	
				var desc            = nsoParseFloatOrZero(discount/CantidadAduana);

				ValorUnitarioAduana = nsoParseFloatOrZero(ValorUnitarioAduana)+nsoParseFloatOrZero(desc);
				ValorDolares        = nsoParseFloatOrZero(ValorDolares)+nsoParseFloatOrZero(discount);
			}
			
			if (wglobaldiscount != null && wglobaldiscount != '' && wglobaldiscount != "NaN"){
					ValorUnitarioAduana = nsoParseFloatOrZero(ValorUnitarioAduana - ((ValorUnitarioAduana*discountmonto)));
					ValorDolares        = nsoParseFloatOrZero(ValorUnitarioAduana*CantidadAduana).toFixed(2);
			}
			//se calcula con el IVA
			if(taxamount > 0){
				ValorUnitarioAduana = nsoParseFloatOrZero(ValorUnitarioAduana)+nsoParseFloatOrZero(taxamount/CantidadAduana);
				ValorDolares        = nsoParseFloatOrZero(ValorDolares)+nsoParseFloatOrZero(taxamount);
			}
			else{
				taxrate = nsoParseFloatOrZero(taxrate/100);
				ValorUnitarioAduana = nsoParseFloatOrZero(ValorUnitarioAduana+(ValorUnitarioAduana*taxrate));
				ValorDolares        = nsoParseFloatOrZero(ValorDolares)+nsoParseFloatOrZero(ValorDolares*taxrate);
			}

			ValorUnitarioAduana = ValorUnitarioAduana.toFixed(2);
			ValorDolares        = ValorDolares.toFixed(2);
			if(moneda != 'USD'){
			    ValorUnitarioAduana = nsoParseFloatOrZero(ValorUnitarioAduana/TipoCambioUSD).toFixed(2);
				ValorDolares        = nsoParseFloatOrZero(ValorDolares/TipoCambioUSD).toFixed(2);
			}
			ValorUnitarioAduana = nsoParseFloatOrZero((ValorUnitarioAduana*tipodecambio)/(TipoCambioUSD))
			ValorDolares        = nsoParseFloatOrZero(CantidadAduana*ValorUnitarioAduana);
			
			//if(xxtype == 'Discount'){CantidadAduana = 1;}
			retvalmercancias += '<fx:Mercancia';
			if(NoIdentificacion != null && NoIdentificacion != ''){
				retvalmercancias += ' NoIdentificacion="' + NoIdentificacion + '"';
			}
			if(FraccionArancelaria != null && FraccionArancelaria != ''){
				retvalmercancias += ' FraccionArancelaria="' + FraccionArancelaria + '"';
			}
			if(CantidadAduana != null && CantidadAduana != ''){
				retvalmercancias += ' CantidadAduana="' + CantidadAduana + '"';
			}
			if(UnidadAduana != null && UnidadAduana != ''){
				retvalmercancias += ' UnidadAduana="' + UnidadAduana + '"';
			}
			if(ValorUnitarioAduana != null && ValorUnitarioAduana != ''){
				retvalmercancias += ' ValorUnitarioAduana="' + ValorUnitarioAduana + '"';
			}
			if(ValorDolares != null && ValorDolares != ''){
				retvalmercancias += ' ValorDolares="' + ValorDolares + '"';
			}
			retvalmercancias +='>';
			
			var DescEspecificas = '';
			var Marca = '';
			var Modelo = '';
			var SubModelo = '';
			var NumeroSerie = '';
			if(  (Marca != null && Marca != '') || (Modelo != null && Modelo != '') || (SubModelo != null && SubModelo != '') || (NumeroSerie != null && NumeroSerie != '')  ){
				
				retvalmercancias += '<fx:DescripcionesEspecificas';    
				if(Marca != null && Marca != ''){
					retvalmercancias += ' Marca="' + Marca + '"';
				}
				if(Modelo != null && Modelo != ''){
					retvalmercancias += ' Modelo="' + Modelo + '"';
				}
				if(SubModelo != null && SubModelo != ''){
					retvalmercancias += ' SubModelo="' + SubModelo + '"';
				}
				if(NumeroSerie != null && NumeroSerie != ''){
					retvalmercancias += ' NumeroSerie="' + NumeroSerie + '"';
				}
				retvalmercancias += '>';
				retvalmercancias += '</fx:DescripcionesEspecificas>';
			}
			retvalmercancias += '</fx:Mercancia>';
		}//if (witemtype == 'InvtPart' ||witemtype == 'Assembly' ||witemtype == 'Kit' ||witemtype == 'Group' || witemtype == 'Markup' ||witemtype == 'NonInvtPart' ||witemtype == 'OthCharge' ||witemtype == 'Payment' ||witemtype == 'Service' )
		if (xxtype == 'Group' && printitems == 'F'){
			bandera = true;		
		}
		if (xxtype == 'EndGroup'){	
			bandera = false;	
			ratelinealdiscountingroup =  '';
		}			
		if (bandera == true){	
			var amountgrup = parseFloat(nlGetAmountGroup(invoice, bandera));		
		}
	}// for (var j=1; j <= invoice.getLineItemCount('item'); j++){
	if (retvalmercancias != null && retvalmercancias != ''){
		retval += '<fx:Mercancias>';
		retval += retvalmercancias;
		retval += '</fx:Mercancias>';
	}
	retval += '</fx:ComercioExterior11>';
	retval += '</fx:Complementos>';
	return retval;

}

function nlGetCFDi01(invoice, customer, params, setupcfdi, ComercioExterior){

    var folio = invoice.getFieldValue('tranid');
	var FolioFiscalOrigUuid = '';//632f736a-35a3-44f2-bae4-dd94e3ebdd53
	var SerieFolioFiscalOrig = '';
	var FechaFolioFiscalOrig = '';
	var MontoFolioFiscalOrig = '';
	var emailsend            = params.getFieldValue('custrecord_cfdi_emailenvio')
	var emailcustomer        = (invoice.getFieldValue(id_mail_customer))||'custbody_email_cliente';;
	var sendemail            = setupcfdi.getFieldValue('custrecord_cfdi_mailalcliente');
	var emailtest            = setupcfdi.getFieldValue('custrecord_cfdi_email_send_test');
	var testing              = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var NumCtaPago           = invoice.getFieldValue('custbody_numctapago');
	var emailemployed        = nlapiGetContext().getEmail();
	var TipoDeComprobante    = (getTipoDocumento(invoice.getRecordType()));
	var MotivoTraslado       = invoice.getFieldText('custbody_cfdi_mottraslado')||'';
	var lugExpedicionCE      = params.getFieldValue('custrecord_cfdi_lugexpedicion_ce');
	var cartaporte           = invoice.getFieldValue('custbody_cfdi_carta_porte')||'F';
	var confirmacion         = invoice.getFieldValue('custbody_cfdi_confirmacion')||'F';
	var CPEmision            = (params.getFieldValue('custrecord_cfdi_cp_expedidoen'));
	var retval = '';
	
	retval += '<fx:Identificacion>';
	retval += '<fx:CdgPaisEmisor>MX</fx:CdgPaisEmisor>';
	if (cartaporte == 'T'){
		retval += '<fx:TipoDeComprobante>TRASLADO</fx:TipoDeComprobante>';
	}
	else{
		retval += '<fx:TipoDeComprobante>' + (getTipoDocumento(invoice.getRecordType()))+ '</fx:TipoDeComprobante>';	
	}
	if(testing == 'T')	{
	 	retval += '<fx:RFCEmisor>' + setupcfdi.getFieldValue('custrecord_cfdi_rfctesting') + '</fx:RFCEmisor>';	
	}
	else	{
		retval += '<fx:RFCEmisor>' + params.getFieldValue('custrecord_cfdi_rfcemisor') + '</fx:RFCEmisor>';	
	} 
	retval += '<fx:RazonSocialEmisor>' + (params.getFieldValue('custrecord_cfdi_nombreemisor')) + '</fx:RazonSocialEmisor>';
	retval += '<fx:Usuario>' + params.getFieldValue('custrecord_cfdi_usuario') + '</fx:Usuario>';
	var tipodedocumento = getTipoDocumento(invoice.getRecordType());
	retval += '<fx:NumeroInterno>' + tipodedocumento + ': '+ folio + '</fx:NumeroInterno>';
	retval += '<fx:LugarExpedicion>'  +  CPEmision + '</fx:LugarExpedicion>';
	if (NumCtaPago != null && NumCtaPago != '' && MotivoTraslado == ''){		
		retval += '<fx:NumCtaPago>' + NumCtaPago + '</fx:NumCtaPago>';	
	}
	retval += '</fx:Identificacion>';
	
	
	
	var cfdirelacionado = ''
	var UUID         = invoice.getFieldValue('custbody_uuid')||'';
	var TipoRelacion = invoice.getFieldText('custbody_cfdi_tipode_relacion')||'';
	var arrayUUID    = new Array();
	var count        = 0;
	var docidUUID    = '';
	
	var relacionadosUUID = GetRelacionados(invoice)||'';
	if (relacionadosUUID.length > 0){
		arrayUUID = relacionadosUUID;
	}
	else{
		for (var j=1; j<= invoice.getLineItemCount('apply'); j++){
			var apply = invoice.getLineItemValue('apply', 'apply', j)||'F';
			if (apply == 'T'){
				var docid       = invoice.getLineItemValue('apply', 'doc', j);
				var docref      = nsoGetTranRecord(docid);
				var idcampoxml  = nlapiGetContext().getSetting('SCRIPT','custscript_id_campo_xml')||'custbody_cfdixml';
				if (idcampoxml != ''){
					var cadenaxml = docref.getFieldValue(idcampoxml)||'';
					if (cadenaxml != ''){
						try{
							var xmlDocument = nlapiStringToXML(cadenaxml);
							var Complemento = nlapiSelectNodes(xmlDocument ,"//*[name()='tfd:TimbreFiscalDigital']" ); 
							docidUUID       = Complemento[0].getAttribute('UUID')||'';
							if(docidUUID != ''){
								arrayUUID[count] = docidUUID;
								count +=1;
							}
						}
						catch(err){}
					}//if (cadenaxml != ''){
				}//if (idcampoxml != ''){
			}//if (apply == 'T'){
		}//for (var j=1; j<= invoice.getLineItemCount('apply'); j++){
	}//else if (relacionadosUUID.length > 0)
	if(arrayUUID.length == 0 && UUID != '') {
		arrayUUID[0] = UUID;
	}

	if(TipoRelacion != '' && arrayUUID.length > 0){
		var arraytiporel = new Array();
		arraytiporel     = TipoRelacion.split('-');
		TipoRelacion     = arraytiporel[0];
		retval += '<fx:CfdiRelacionados>';	
		retval += '<fx:TipoRelacion>' + TipoRelacion + '</fx:TipoRelacion>';
		for (var a= 0; a < arrayUUID.length; a++){
			retval += '<fx:UUID>' + arrayUUID[a] + '</fx:UUID>';
		}
		retval += '</fx:CfdiRelacionados>';	
	}


	//------> Envio de correo a cuenta de pruebas
	if (testing == 'T' ){
		if(emailtest != null && emailtest != '') {
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			retval += '<fx:Entry k="to" v="' +emailtest+'"/>';
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	else {
		//------> Envio de correo al cliente
		if (sendemail == 'T'){	
			if ((emailsend != null && emailsend != '')||(emailcustomer != null && emailcustomer != '')){
				retval += '<fx:Procesamiento>';
				retval += '<fx:Dictionary name="email">';
				if (emailcustomer != null && emailcustomer != '' && emailsend != null && emailsend != ""){
					retval += '<fx:Entry k="to" v="' + emailcustomer + ';'+ emailsend +'"/>';
				}
				else if((emailcustomer == null || emailcustomer == '') && emailsend != null && emailsend != ""){
					retval += '<fx:Entry k="to" v="' +emailsend+'"/>';
				}
				else if((emailsend == null || emailsend == '') && emailcustomer != null && emailcustomer != ""){
					retval += '<fx:Entry k="to" v="' + emailcustomer +'"/>';
				}
				retval += '</fx:Dictionary>';
				retval += '</fx:Procesamiento>';
			}
		}
		else{
			retval += '<fx:Procesamiento>';
			retval += '<fx:Dictionary name="email">';
			if (emailsend != null && emailsend != ""){
				retval += '<fx:Entry k="to" v="' + emailsend +'"/>';
			}		
			retval += '</fx:Dictionary>';
			retval += '</fx:Procesamiento>';
		}
	}
	
		
	//----> Datos Emisor	
	retval     += '<fx:Emisor>';	
	retval     += '<fx:DomicilioFiscal>';	
	var calle   = (params.getFieldValue('custrecord_cfdi_calleemisor'));	
	retval     += '<fx:Calle>' + calle + '</fx:Calle>'; 	   
	var numext = isNull(params.getFieldValue('custrecord_cfdi_numexterioremisor'));
	if (numext != null && numext != ''){
		retval += '<fx:NumeroExterior>' + numext + '</fx:NumeroExterior>'; 	    
	}	
	var numint = isNull(params.getFieldValue('custrecord_cfdi_numinterioremisor'));
	if (numint != null && numint != ''){																								
   	    retval += '<fx:NumeroInterior>' + numint + '</fx:NumeroInterior>'; 
	}	
	var localidad = isNull(params.getFieldValue('custrecord_cfdi_localidademisor'));
	if (localidad != null && localidad != '' ){
    	retval += '<fx:Localidad>' + localidad + '</fx:Localidad>';    
	}	
	var referencia = isNull(params.getFieldValue('custrecord_cfdi_referenciaemisor'));
	if (referencia != null && referencia != ''){
		retval += '<fx:Referencia>' + referencia + '</fx:Referencia>';   
	}	
	var colonia = isNull(params.getFieldValue('custrecord_cfdi_coloniaemisor'));
	if (colonia != null && colonia != ''){
    	retval += '<fx:Colonia>' + colonia + '</fx:Colonia>'; 
	}

	
	var municipio = (params.getFieldValue('custrecord_cfdi_municipioemisor'));
	if (municipio != null && municipio != ''){
		retval += '<fx:Municipio>'  + municipio + '</fx:Municipio>';   
	}
	retval += '<fx:Estado>' + (params.getFieldValue('custrecord_cfdi_estadoemisor')) + '</fx:Estado>';	    
	retval += '<fx:Pais>'   + (params.getFieldValue('custrecord_cfdi_paisemisor'))   + '</fx:Pais>'; 	    
	retval += '<fx:CodigoPostal>'  + (params.getFieldValue('custrecord_cfdi_cpemisor')) + '</fx:CodigoPostal>'; 	
	var Telefono = (params.getFieldValue('custrecord_cfdi_telcorporativo'));
	if (Telefono != null && Telefono != ''){
		retval += '<fx:TelContacto>'  + Telefono+ '</fx:TelContacto>';  	
	}
	 		
	retval += '</fx:DomicilioFiscal>';	
	
	//------------->Empiezan datos del lugar de emision <--------------------------------// 
	
	var estadoemision = (params.getFieldValue('custrecord_cfdi_estado_expedidoen'));
	var paisemision = (params.getFieldValue('custrecord_cfdi_pais_expedidoen'));
	var CPEmision = (params.getFieldValue('custrecord_cfdi_cp_expedidoen'));
	
	if( (estadoemision != null && estadoemision != '') && (paisemision != null && paisemision != '') && (CPEmision != null && CPEmision != '') )
	{
		retval += '<fx:DomicilioDeEmision>';
			
		var calleemi = (params.getFieldValue('custrecord_cfdi_calleexpedidoen'));
		if (calleemi != null && calleemi != '')
		{
			retval += '<fx:Calle>' + calleemi + '</fx:Calle>'; 	   
		}	
		var numextemi = (params.getFieldValue('custrecord_cfdi_numext_expedidoen'));
		if (numextemi != null && numextemi != '')
		{
			retval += '<fx:NumeroExterior>' + numextemi + '</fx:NumeroExterior>'; 	    
		}	
		var numintemi = (params.getFieldValue('custrecord_cfdi_numint_expedidoen'));
		if (numintemi != null && numintemi != '')
		{																								
			retval += '<fx:NumeroInterior>' + numintemi + '</fx:NumeroInterior>'; 
		}	
		var localidademi = (params.getFieldValue('custrecord_cdfi_loc_expedidoen'));
		if (localidademi != null && localidademi != '' ){
			retval += '<fx:Localidad>' + localidademi + '</fx:Localidad>';    
		}	
		var referenciaemi = (params.getFieldValue('custrecord_cfdi_ref_expedidoen'));
		if (referenciaemi != null && referenciaemi != ''){
			retval += '<fx:Referencia>' + referenciaemi + '</fx:Referencia>';   
		}	
		var coloniaemi = (params.getFieldValue('custrecord_cfdi_colonia_expedidoen'));
		
		retval += '<fx:Colonia>' + coloniaemi + '</fx:Colonia>'; 
		var municipioemi = (params.getFieldValue('custrecord_cfdi_mun_expedidoen'));
		if (municipioemi != null && municipioemi != ''){
			retval += '<fx:Municipio>'  + municipioemi + '</fx:Municipio>';   
		}
		retval += '<fx:Estado>' + estadoemision + '</fx:Estado>';	    
		retval += '<fx:Pais>'  + paisemision + '</fx:Pais>'; 	    
		retval += '<fx:CodigoPostal>' + CPEmision + '</fx:CodigoPostal>';  		
		retval += '</fx:DomicilioDeEmision>';
	}
		
	retval += '<fx:RegimenFiscal>';
	var regfisc = (params.getFieldText('custrecord_cfdi_regimenfiscal_ce'));
	var regArray = regfisc.split('-');
	var Regimen  = regArray[0];
	retval += '<fx:Regimen>' + Regimen + '</fx:Regimen>'
	retval += '</fx:RegimenFiscal>';	    	
	retval += '</fx:Emisor>';	
	retval  = retval.replace(/&/g, "&amp;");
	
	
    return retval;
}

function nlGetCFDiH4(invoice, customer, params, setupcfdi, ComercioExterior){
  	var retval = '';
    var billaddresslist = invoice.getFieldValue("billaddresslist");
	var shipaddresslist = (invoice.getFieldValue("shipaddresslist"));
	var recRFC = invoice.getFieldValue(id_campo_rfc);
	if ( recRFC != null && recRFC != '')
	{
		recRFC = recRFC.replace(/-/g, "");
		recRFC = recRFC.replace(/\s/g, "");
		
	}
	var nombreenvio = (customer.getFieldValue('custentity_kv_razon_social'));
	var retvalreceptor = '';
	var retvalDomRec = '';
	var NumRegIdTrib         = customer.getFieldValue('custentity_numregidtrib');
	if (ComercioExterior == 'F'){
		var NumRegIdTrib = '';
	}
  	if (billaddresslist == null || billaddresslist == '')  
	{
		//retvalreceptor = '<fx:Receptor>';		
		var calleenvio     = (invoice.getFieldValue('billaddr1'));
		var coloniaenvio   = (invoice.getFieldValue('billaddr2'));
		var municipioenvio = (invoice.getFieldValue('billcity'));
		var estadoenvio    = (invoice.getFieldValue('billstate'));
		var cpenvio        = (invoice.getFieldValue('billzip'));
		var cpenvioanex    = (invoice.getFieldValue('custrecord_cfdi_anexo_cp'));
		var codigopais     = (invoice.getFieldValue('billcountry'));
		var pais           = nsoGetCountryName(invoice.getFieldValue('billcountry'));
		var paisCE         = nsoGetCountryNameComExt(invoice.getFieldValue('billcountry'));
		
		retvalreceptor += '<fx:CdgPaisReceptor>'+ codigopais +'</fx:CdgPaisReceptor>';
		retvalreceptor += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>';  
		if (NumRegIdTrib != null && NumRegIdTrib != '')
		{retvalreceptor += '<fx:TaxID>' + NumRegIdTrib + '</fx:TaxID>';}				
		if (nombreenvio != null && nombreenvio != '')
		{retvalreceptor += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}	
		
		
		if ((calleenvio != null && calleenvio != '')||(municipioenvio != null && municipioenvio != '') || (estadoenvio != null && estadoenvio != '') ||(pais != null && pais != '') || (cpenvio != null && cpenvio != '') ){
			
			retvalreceptor += '<fx:Domicilio>';
			if (codigopais != 'MX')	{retvalreceptor += '<fx:OtroDomicilio>';}
			else
			{retvalreceptor += '<fx:DomicilioFiscalMexicano>';}
			if (calleenvio != null && calleenvio != ''){
				retvalreceptor += '<fx:Calle>' + isNull(calleenvio) + '</fx:Calle>'; } 
			if (coloniaenvio != null && coloniaenvio != ''){
				retvalreceptor += '<fx:Colonia>' + coloniaenvio + '</fx:Colonia>';}
			if (municipioenvio != null && municipioenvio != ''){	
				retvalreceptor += '<fx:Municipio>' + municipioenvio + '</fx:Municipio>';  }
			if (estadoenvio != null && estadoenvio != ''){
				retvalreceptor += '<fx:Estado>' + estadoenvio + '</fx:Estado>';  }
			if (pais != null && pais != ''){
				retvalreceptor += '<fx:Pais>'+ pais +'</fx:Pais>';  }
			if (cpenvio != null && cpenvio != ''){ 
				retvalreceptor += '<fx:CodigoPostal>' + cpenvio + '</fx:CodigoPostal>';   }
			if (codigopais != 'MX')	{
				retvalreceptor += '</fx:OtroDomicilio>';}
			else{
				retvalreceptor += '</fx:DomicilioFiscalMexicano>';
			}
			retvalreceptor += '</fx:Domicilio>';
		}
		//retval += retvalreceptor;
		//retval += '</fx:Receptor>';
		
	}
 	else{
		
		for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){
			
			if (customer.getLineItemValue("addressbook", "id", i) == billaddresslist){
				
				var codigopais    = (customer.getLineItemValue('addressbook', 'country', i));
				var pais          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
				var callereceptor = (customer.getLineItemValue('addressbook', 'addr1', i));
				var colonia       = (customer.getLineItemValue('addressbook', 'addr2', i));
				var Municipio     = (customer.getLineItemValue('addressbook', 'city', i));
				var Estado        = (customer.getLineItemValue('addressbook', 'state', i));
				var CP            = (customer.getLineItemValue('addressbook', 'zip', i));		
				customer.selectLineItem('addressbook', i);
				var subrecord     = customer.viewCurrentLineItemSubrecord('addressbook','addressbookaddress');
				var CPanexo       = subrecord.getFieldValue('custrecord_cfdi_anexo_cp');
				var paisCE        = nsoGetCountryNameComExt(customer.getLineItemValue('addressbook', 'country', i));
				if (codigopais == 'MEX' && ComercioExterior == 'T')	{
					var colonia    = subrecord.getFieldValue('custrecord_colonia_ce');
					var Estado     = subrecord.getFieldValue('custrecord_estado_ce');
					var Municipio  = subrecord.getFieldValue('custrecord_localidad_ce');
					var localidad  = subrecord.getFieldValue('custrecord_municipio_ce');
					
				}				
				
				if (codigopais != null && codigopais != ''){
					
					retvalreceptor += '<fx:CdgPaisReceptor>' + codigopais + '</fx:CdgPaisReceptor>';
					retvalreceptor += '<fx:RFCReceptor>' + recRFC + '</fx:RFCReceptor>'; 
					if (NumRegIdTrib != null && NumRegIdTrib != '')
					{retvalreceptor += '<fx:TaxID>' + NumRegIdTrib + '</fx:TaxID>';}		
					if (nombreenvio != null && nombreenvio != '')
					{retvalreceptor += '<fx:NombreReceptor>' + nombreenvio + '</fx:NombreReceptor>';}
				}
				if ((callereceptor != null && callereceptor != '')||(Municipio != null && Municipio != '') || (Estado != null && Estado != '') ||(pais != null && pais != '') || (CP != null && CP != '') ){
					retvalreceptor += '<fx:Domicilio>';
					
					if (codigopais != 'MX')	
					{retvalreceptor += '<fx:OtroDomicilio>';}
					else
					{retvalreceptor += '<fx:DomicilioFiscalMexicano>';}
					if (callereceptor != null && callereceptor != '')
					{retvalreceptor += '<fx:Calle>' + callereceptor + '</fx:Calle>';}   
					if (colonia != null && colonia != '')
					{retvalreceptor += '<fx:Colonia>' + colonia + '</fx:Colonia>';}
					if (Municipio != null && Municipio !='')
					{retvalreceptor += '<fx:Municipio>' + Municipio + '</fx:Municipio>';}
					if (Estado != null && Estado != '')
					{retvalreceptor += '<fx:Estado>' + Estado + '</fx:Estado>';}
					if (pais != null && pais != '')
					retvalreceptor += '<fx:Pais>' + pais + '</fx:Pais>';           
					if (CP != null && CP != '')
					{retvalreceptor += '<fx:CodigoPostal>' + CP + '</fx:CodigoPostal>';} 
					if (codigopais != 'MX')	
					{retvalreceptor += '</fx:OtroDomicilio>';}
					else
					{retvalreceptor += '</fx:DomicilioFiscalMexicano>';}
					
					retvalreceptor += '</fx:Domicilio>';
					//nlapiLogExecution('DEBUG','retvalreceptor ', retvalreceptor); 
				}
			}//end 
		}// end for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) 
		for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++){	
			
			if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist)
			{
				var codigopaisenvio    = (customer.getLineItemValue('addressbook', 'country', i));
				var paisenvio          = nsoGetCountryName(customer.getLineItemValue('addressbook', 'country', i));
				var callereceptorenvio = (customer.getLineItemValue('addressbook', 'addr1', i));
				var coloniaenvio       = (customer.getLineItemValue('addressbook', 'addr2', i));
				var Municipioenvio     = (customer.getLineItemValue('addressbook', 'city', i));
				var Estadoenvio        = (customer.getLineItemValue('addressbook', 'state', i));
				var CPenvio            = (customer.getLineItemValue('addressbook', 'zip', i));	
				customer.selectLineItem('addressbook', i);
				var subrecord          = customer.viewCurrentLineItemSubrecord('addressbook','addressbookaddress');
				var CPenvioanexo       = subrecord.getFieldValue('custrecord_cfdi_anexo_cp');
				var retvalenvio        = '';
				var paisCEenvio        = nsoGetCountryNameComExt(customer.getLineItemValue('addressbook', 'country', i));		
				if (codigopaisenvio == 'MEX' && ComercioExterior == 'T')	{
					var coloniaenvio    = subrecord.getFieldValue('custrecord_colonia_ce');
					var Estadoenvio     = subrecord.getFieldValue('custrecord_estado_ce');
					var Municipioenvio  = subrecord.getFieldValue('custrecord_localidad_ce');
					var localidadenvio  = subrecord.getFieldValue('custrecord_municipio_ce');
				}

				retvalDomRec += '<fx:DomicilioDeRecepcion>';
			
				if (codigopaisenvio != 'MX')	
				{retvalDomRec += '<fx:OtroDomicilio>';}
				else
				{retvalDomRec += '<fx:DomicilioFiscalMexicano>';}
				if (callereceptorenvio != null && callereceptorenvio != '')
				{retvalDomRec += '<fx:Calle>' + callereceptorenvio + '</fx:Calle>';}   
				if (coloniaenvio != null && coloniaenvio != '')
				{retvalDomRec += '<fx:Colonia>' + coloniaenvio + '</fx:Colonia>';}
				if (Municipioenvio != null && Municipioenvio !='')
				{retvalDomRec += '<fx:Municipio>' + Municipioenvio + '</fx:Municipio>';}
				if (Estadoenvio != null && Estadoenvio != '')
				{retvalDomRec += '<fx:Estado>' + Estadoenvio + '</fx:Estado>';}
				if (paisenvio != null && paisenvio != '')
				retvalDomRec += '<fx:Pais>' + paisenvio + '</fx:Pais>';           
				if (CPenvio != null && CPenvio != '')
				{retvalDomRec += '<fx:CodigoPostal>' + CPenvio + '</fx:CodigoPostal>';} 
				if (codigopaisenvio != 'MX')	
				{retvalDomRec += '</fx:OtroDomicilio>';}
				else
				{retvalDomRec += '</fx:DomicilioFiscalMexicano>';}
				retvalDomRec += '</fx:DomicilioDeRecepcion>';
				//nlapiLogExecution('DEBUG','retvalDomRec ', retvalDomRec); 
				//break;
			}// end if (customer.getLineItemValue("addressbook", "id", i) == shipaddresslist)
		}// end for (var i = 1; i <= customer.getLineItemCount('addressbook'); i++) 
		
	}// end else
	if ((retvalreceptor != null && retvalreceptor != '')||(retvalDomRec != null && retvalDomRec != '')){
			retval += '<fx:Receptor>';	
			retval += retvalreceptor;
			retval += retvalDomRec;

			if (codigopais != 'MX' ){
				retval += '<fx:ResidenciaFiscal>' + paisCE + '</fx:ResidenciaFiscal>'; 
			}

			var usocfdi  = invoice.getFieldText("custbody_uso_cfdi")||'';
			var usoclave = '';
			if (usocfdi != ''){
				var usoArray = usocfdi.split('-');
				var usoclave = usoArray[0];	
			}
			
			retval += '<fx:UsoCFDI>' + usoclave + '</fx:UsoCFDI>'; 
			
			retval += '</fx:Receptor>';
		}
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
    return retval;
}

function nlGetCFDi02(invoice, customer, params, setupcfdi, ComercioExterior, anticipo){
    var retval          = '';
    var wdiscountitem   = invoice.getFieldText('discountitem');
	var wgiftrate       = nsoParseFloatOrZero(invoice.getFieldValue('giftcertapplied'));
	if (wgiftrate != 0){
    	wgiftrate       = 1-( nsoParseFloatOrZero(invoice.getFieldValue('total')) / ( nsoParseFloatOrZero(invoice.getFieldValue('subtotal')) +  nsoParseFloatOrZero(invoice.getFieldValue('taxtotal'))));
	}
	var wglobaldiscount = Math.abs(parseFloat(isNull2(invoice.getFieldValue('discounttotal'), 0)));
	var wglobalsubtotal = Math.abs(parseFloat(isNull2(invoice.getFieldValue('subtotal'), 0))); 
	wglobaldiscount     = nsoParseFloatOrZero(wglobaldiscount) //+ nsoParseFloatOrZero(gift/1.16);
    var wdiscountrate   = (wglobaldiscount*100/(wglobalsubtotal));
	wdiscountrate       = isNaN(wdiscountrate) ? 0 : parseFloat(wdiscountrate);
	if (wdiscountrate != null && wdiscountrate != '')
	{wdiscountrate = wdiscountrate.toFixed(4);}
    var bandera         = false;
    var j               = 1;
	var wnum_partida    = 0;
	var printitems      = null;
	var amountgroup     = '';
	var discountingroup = 0;
	var ratelinealdiscountingroup = '';
	var subtotal        = 0;
	var subtotalbruto   = 0;    
	var numline         = 0;
	var numlinetax      = 0;
	var numlineretISR   = 0;
	var numlineretIVA   = 0;
	this.tasades 		= new Array();
	this.tasatax 		= new Array();
	this.TasaRetencion  = new Array();
	var descuentototal  = 0;
	var taxexento       = (setupcfdi.getFieldValue('custrecord_cfdi_taxexento'))||'';
	var taxcero         = (setupcfdi.getFieldValue('custrecord_cfdi_taxexento'))||'';
	
	retval             += '<fx:Conceptos>';
	var comext          = invoice.getFieldValue('custbody_cfdi_comercio_exterior');
	var idcustomformCE      = nlapiGetContext().getSetting('SCRIPT','custscript_id_customform_cext');
	var customform  = invoice.getFieldValue("customform");
	var AduanasNumber   = CatalogoAduanas();
	var ClaveUnidMed    = UnidadMedida();
	var retencion       = ''; 
	var newtax16        = 0;
	var newRetax16      = 0;
	var tipodedocumento = getTipoDocumento(invoice.getRecordType());
	var kititems        = SearchkitItems(invoice)||'';
	var cartaporte      = invoice.getFieldValue('custbody_cfdi_carta_porte')||'F';
	
	while (j <= invoice.getLineItemCount('item')){
		var discount  = 0; 
		var witemid   = invoice.getLineItemValue('item', 'item', j);
		var xxtype    = invoice.getLineItemValue('item', 'itemtype', j);
		var witemtype = xxtype;
		var line      = parseFloat(invoice.getLineItemValue('item', 'line', j));
		
		if (witemtype == '' && xxtype == 'EndGroup'){
			witemtype = xxtype
		}
		
		//---> Obtiene el valor para ver si se impriman los articulos de grupo en la factura
		var ClaveProdServgroup  = '';
		var UnidadMedida_group  = '';
		if (witemtype == 'Group'){
			var record = nlapiLoadRecord('itemgroup', witemid);
			printitems = record.getFieldValue('printitems');
			line = line+1;
			if (printitems != 'T'){
				if (id_clave_prodse_group){
					var ClaveProdServgroup  = record.getFieldValue(id_clave_prodse_group);
				}
				if (id_UnidadLine_group){
					var UnidadMedida_group  = record.getFieldValue(id_UnidadLine_group );
				}
			}
		}
		if (xxtype == 'Group' && printitems == 'T'){
			ratelinealdiscountingroup =  Math.abs(parseFloat(isNull(RateDiscountGroup(j, invoice))));
		}
		if (xxtype == 'EndGroup' && printitems == 'T'){
			ratelinealdiscountingroup =  '';
		}
		
		if (xxtype == 'Group' && printitems == 'T'){	
			j=j+1;		
		}		
		
			
		if (bandera == false)
		{		
			//--> Obtiene Informacion Detalle Factura
			var witemdiscounts = 0;
			var wdiscounts	   = new Array();
			var wtax		   = new Array();
		  	var itemidtext     = invoice.getLineItemText('item', 'item', j);
			var wdescitem      = invoice.getLineItemValue('item', 'description', j);
			//var descArray      = new Array();
			//descArray          = wdescitem.split('\n');
			var wamount        = parseFloat(invoice.getLineItemValue('item', 'amount', j));  
			var wtaxamt        = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'tax1amt', j));
			var wquantity      = Math.abs(parseFloat(invoice.getLineItemValue('item', 'quantity', j)));  
			var wrate          = parseFloat(invoice.getLineItemValue('item', 'rate', j)); 		
			var wtaxrate       = parseFloat(invoice.getLineItemValue('item', 'taxrate1', j));
			var taxcode        = parseFloat(invoice.getLineItemValue('item', 'taxcode', j));  
			if (witemtype == 'markupitem') {var wquantity = 1;}
			
			var wcodigo    = (invoice.getLineItemValue('item', 'item', j));
			if (wcodigo != null && wcodigo != ''){
				if(wcodigo.length > 30){
				   wcodigo = wcodigo.substr(0,30);
				}
			}
			
			var medida         = (invoice.getLineItemText('item', 'units', j));
			var idieps         = (setupcfdi.getFieldValue('custrecord_cfdi_idieps'));
			var ClaveProdServ  = (invoice.getLineItemValue('item', 'custcol_cfdi_clave_prod_serv', j))||'';
			var ClaveUnidad    = (invoice.getLineItemValue('item', 'custcol_cfdi_clave_unidad', j))||'';
			var UnidadLine     = (invoice.getLineItemValue('item', 'units', j));
			if (xxtype == 'Group'){
				wtaxrate = parseFloat(invoice.getLineItemValue('item', 'taxrate1', j+1));

			}
			if (witemtype == 'Group' && printitems == 'F'){
				var ClaveProdServ  = ClaveProdServgroup;
				var UnidadLine     = UnidadMedida_group;
			}
			var printitemskit = '';
			if (witemtype == 'Kit'){
				var record = nlapiLoadRecord('kititem', witemid);
				var ClaveProdServgroup  = record.getFieldValue(id_clave_prodse_group);
				var UnidadMedida_group  = record.getFieldValue(id_UnidadLine_group );
				UnidadLine     = UnidadMedida_group;
				ClaveProdServ  = ClaveProdServgroup;
				printitemskit  = record.getFieldValue('printitems')||'';
			}
			var UnidadAduana   = '';
			for (var c = 0; ClaveUnidMed  != '' && c< ClaveUnidMed.length; c++ ){
				var units = ClaveUnidMed[c].getValue('custrecord_cfdi_nat_units');
				if (units == UnidadLine){
					UnidadAduana = ClaveUnidMed[c].getValue('custrecord_cfdi_clav_unidad');
				}
			}
						
			var taxdiscount   = 0;
			var discountarray = DiscountForItem(line, invoice)||'';		
			if (discountarray != ''){
				discount	= nsoParseFloatOrZero(discountarray[0]);
				wtaxamt     += nsoParseFloatOrZero(discountarray[1]);
			}
			
			var retvalTaxSAT = '';
			var retvalRetTaxSAT = '';
			
			if(witemtype == 'Kit'){
				if(printitemskit == 'F'){
					
				}//if(printitemskit == 'F'){
			}//if(witemtype == 'Kit'){
			
			if (witemtype == 'InvtPart' || witemtype == 'Assembly'  || witemtype == 'Group' || witemtype == 'Markup' || witemtype == 'NonInvtPart' || witemtype == 'OthCharge' || witemtype == 'Payment' || witemtype == 'Service' || witemtype == 'Kit'){
				//-----> Numeros de serie
				var wnumparte      = invoice.getLineItemValue('item', 'custcol_numpart', j);
				if (wnumparte != null && wnumparte != ''){
					wnumparte = wnumparte.replace(/\n/g, "");
				}
				var wserialnumbers = invoice.getLineItemValue('item', 'serialnumbers', j); 		
				
				if(witemtype == 'lotnumberedinventoryitem' || witemtype == 'lotnumberedassemblyitem'){
					wserialnumbers = "";
				}
				
				if(wserialnumbers == null || wserialnumbers == "")	{
					var arrseries = ArraySerialNumber(invoice);					
					wserialnumbers = ObtSerialNumbersOrPedimento(arrseries, wnumparte);					
				}
				else{
					wserialnumbers = replace(wserialnumbers, String.fromCharCode(5), ",");					
				}
				
				var xserials = wserialnumbers.split(","); 
				var yserial = AgroupSerials(xserials);				
				
				retval += '<fx:Concepto>';	
				if (anticipo == 'T'){wquantity = 1;}
				retval += '<fx:Cantidad>' + (wquantity) + '</fx:Cantidad>'; 
				if(tipodedocumento == 'NOTA_DE_CREDITO' && cambioValoresNC == 'T'){
					UnidadAduana  = params.getFieldValue('custrecord_cfdi_claveunidad_nc')||'ACT'
					ClaveProdServ = params.getFieldValue('custrecord_cfdi_claveprodserv_nc')||'84111506'
				}
				if (anticipo == 'T'){
					UnidadAduana  = params.getFieldValue('custrecord_cfdi_claveunidad_anticipo')||'ACT';
					wdescitem     = params.getFieldValue('custrecord_cfdi_descripcion_anticipo')||'Anticipo del bien o servicio';
					ClaveProdServ = params.getFieldValue('custrecord_cfdi_claveprodserv_anticipo')||'84111506';
				}
				retval += '<fx:ClaveUnidad>' + UnidadAduana + '</fx:ClaveUnidad>'; 	 
				if (medida != null && medida != '' && anticipo != 'T'){
					retval += '<fx:UnidadDeMedida>' + medida + '</fx:UnidadDeMedida>';
				}
				retval += '<fx:ClaveProdServ>' + ClaveProdServ   + '</fx:ClaveProdServ>';
				
				if (wcodigo != null && wcodigo != '' && anticipo != 'T')
				{	retval += '<fx:Codigo>' + wcodigo +'</fx:Codigo>';	}
										
				retval += '<fx:Descripcion>' + (wdescitem) +  '</fx:Descripcion>'; 
				
				var discountmonto    = nsoParseFloatOrZero(wdiscountrate/100);
				
				var amounttaxgroup   = 0;
				var taxdiscountgroup = 0;
				//--------------esto es por si es de grupo y solo se necesita el grupo y su total sin incluir los articulos
				if (witemtype == 'Group' && printitems == 'F'){
					for (var i = j+1; i <= invoice.getLineItemCount('item'); i++){
						var wtype = invoice.getLineItemValue('item', 'itemtype', i);
						if (wtype == 'EndGroup'){
							amountgroup = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', i));
							var line    = parseFloat(invoice.getLineItemValue('item', 'line', i));
							break;
						}
						if  (wtype != 'EndGroup'){
							amounttaxgroup += nsoParseFloatOrZero(invoice.getLineItemValue('item', 'tax1amt', i))
						}
					}
					wrate = parseFloat(amountgroup/wquantity);
					wamount = amountgroup;
					nlapiLogExecution('DEBUG', 'wrate dentro del itemgroup && printitems es falso): ', wrate);
					var discountingrouparray = DiscountForItem(line, invoice);
					if ( discountingrouparray != ''){
						discountingroup    = nsoParseFloatOrZero(discountingrouparray[0]);
						taxdiscountgroup   = nsoParseFloatOrZero(discountingrouparray[1]);
						amounttaxgroup     = nsoParseFloatOrZero(discountingrouparray[0]);
						wtaxamt            += nsoParseFloatOrZero(discountingrouparray[1]);
						nlapiLogExecution('DEBUG', 'discountingroup: ', discountingroup);
					}
				}// end of if (witemtype == 'itemgroup' && printitems == 'F')
				wtaxamt = Math.abs(wtaxamt);
				wamount += discount + discountingroup;
				
				var descglobal = 0;
				if (discountmonto != 0){
					descglobal -= (nsoParseFloatOrZero(wamount*discountmonto));
					wtaxamt    +=  nsoParseFloatOrZero(descglobal*wtaxrate/100);
				}
				wamount += descglobal;
				
				if (ComercioExterior == 'F'){
					if (amountgroup > 0){
						retval   += '<fx:ValorUnitario>' + nsoParseFloatOrZero(wrate).toFixed(4) + '</fx:ValorUnitario>';
						retval   += '<fx:Importe>' + nsoParseFloatOrZero(wrate*wquantity).toFixed(4) + '</fx:Importe>';
					}
					else{
						retval   += '<fx:ValorUnitario>' + nsoParseFloatOrZero(wrate).toFixed(4) + '</fx:ValorUnitario>';
						retval   += '<fx:Importe>' + nsoParseFloatOrZero((invoice.getLineItemValue('item', 'amount', j))).toFixed(4) + '</fx:Importe>';
					}
				}else{
					if (amountgroup > 0){
						retval   += '<fx:ValorUnitario>' + nsoParseFloatOrZero(wrate).toFixed(2) + '</fx:ValorUnitario>';
						retval   += '<fx:Importe>' + nsoParseFloatOrZero(wrate*wquantity).toFixed(2) + '</fx:Importe>';
					}
					else{
						retval   += '<fx:ValorUnitario>' + nsoParseFloatOrZero(wrate).toFixed(2) + '</fx:ValorUnitario>';
						retval   += '<fx:Importe>' + nsoParseFloatOrZero((invoice.getLineItemValue('item', 'amount', j))).toFixed(2) + '</fx:Importe>';
					}
				}
				subtotal += nsoParseFloatOrZero((invoice.getLineItemValue('item', 'amount', j)))+ nsoParseFloatOrZero(amountgroup);
				
				var ConceptoEx   = '';
				var Opciones     = '';
				if (wgiftrate > 0){
					discount = nsoParseFloatOrZero(( wamount+ wtaxamt)* wgiftrate);
				}
					
				var descuento    = Math.abs(nsoParseFloatOrZero(discount + discountingroup + descglobal)).toFixed(4);
				descuentototal   += nsoParseFloatOrZero(descuento);
				
				var pedi    = invoice.getLineItemValue('item', 'custcol_pedimentos', j);			
				var arrpedi = new Array();

								
				if (pedi != null && pedi != ''){
					var arrpedi = new Array();
					if(pedi != null && pedi != ""){	
						pedi = nsoReplace(pedi,"-",'');
						arrpedi = pedi.split(',');
					}
				
					var fechaped = invoice.getLineItemValue('item', 'custcol_fechapedimento', j);
					//nlapiLogExecutiAon('DEBUG', 'fechaped: ', fechaped);
					var arrfechaped = new Array();
					if (fechaped != null && fechaped != ''){
						arrfechaped = fechaped.split(',');
					}
					
					var naduana = invoice.getLineItemValue('item', 'custcol_aduana', j);
					
					var arrnaduana = new Array();
					if (naduana != null && naduana != ''){
						arrnaduana = naduana.split(',');
					}
					
					Opciones += '<fx:DatosDeImportacion>';	
					for (g = 0; arrpedi != null && g < arrpedi.length; g++){
						Opciones += '<fx:InformacionAduanera>';
						Opciones += '<fx:NumeroDePedimento>' + arrpedi[g] + '</fx:NumeroDePedimento>';
						if(arrfechaped[g] != '' && arrfechaped[g] != null){
							Opciones += '<fx:FechaDePedimento>' + arrfechaped[g] + '</fx:FechaDePedimento>';
						}
						var numeroaduana = '';
						for (var a = 0 ; AduanasNumber != '' && a < AduanasNumber.length; a++ ){
							var name = AduanasNumber[a].getText('custrecord_cfdi_aduana_name');
							if (name  == arrnaduana[g]){
								var aduana = AduanasNumber[a].getText('custrecord_cfdi_aduana_clave');
								var aduanaarray = aduana.split('-');
								var numeroaduana = aduanaarray[0];
								break;
						    }
						}
						if(numeroaduana != '' && numeroaduana != null){						
							Opciones += '<fx:NombreDeAduana>' + numeroaduana + '</fx:NombreDeAduana>';
						}
						Opciones += '</fx:InformacionAduanera>';
					}
					Opciones += '</fx:DatosDeImportacion>';
					
				}//end of Pedi != null
				
				if (printitemskit == 'T'){
					
					for (var k = 0;kititems != null && k < kititems.length; k++){
						var kitiditem = kititems[k].getId();
						if(kitiditem === witemid){
							var ClaveProdServKit    = kititems[k].getValue('custitem_clave_prod_serv', 'memberItem')|'';
							var CantidadKitItem     = kititems[k].getValue('memberquantity')||'';
							var Descripcionkit      = kititems[k].getText( 'memberItem')||'';
							var Unidadkit           = kititems[k].getText('saleunit', 'memberItem')||'';
							var NoIdentificacionkit = kititems[k].getValue('saleunit', 'memberItem')||'';
							var ValorUnitariokit    = '';
							var Importekit          = '';
							Opciones += '<fx:Parte ';
							if(ClaveProdServKit != ''){
								Opciones += ' ClaveProdServ="' + ClaveProdServKit + '"';
							}
							if(NoIdentificacionkit != ''){
								Opciones += ' NoIdentificacion="' + NoIdentificacionkit + '"';
							}
							if(CantidadKitItem != ''){
								Opciones += ' Cantidad="' + CantidadKitItem + '"';
							}
							if(Unidadkit != ''){
								Opciones += ' Unidad="' + Unidadkit + '"';
							}
							if(Descripcionkit != ''){
								Opciones += ' Descripcion="' + Descripcionkit + '"';
							}
							if(ValorUnitariokit != ''){
								Opciones += ' ValorUnitario="' + ValorUnitariokit + '"';
							}
							if(Importekit != ''){
								Opciones += ' Importe="' + Importekit + '"';
							}
							Opciones +='/>';
						}//if(kitiditem === witemid){
					}//for (var k = 0; k < kititems.length; k++){
				}//if (printitemskit == 'T'){

				if (witemtype == 'Group' && printitems == 'F'){
					var Plista =  parseFloat(amountgroup/wquantity);
					//wamount = amountgroup;
					if (ComercioExterior == 'F'){
						ConceptoEx       += '<fx:PrecioLista>' + nsoParseFloatOrZero(Plista).toFixed(6) + '</fx:PrecioLista>'; 	
						ConceptoEx       += '<fx:ImporteLista>' + parseFloat(amountgroup).toFixed(6) + '</fx:ImporteLista>';	
					}
					else{
						ConceptoEx       += '<fx:PrecioLista>' + nsoParseFloatOrZero(Plista).toFixed(2) + '</fx:PrecioLista>'; 		
						ConceptoEx       += '<fx:ImporteLista>' + parseFloat(amountgroup).toFixed(2) + '</fx:ImporteLista>';	
					}
					subtotalbruto = parseFloat(subtotalbruto) + parseFloat(amountgroup);	
				}
				else{
					var preciolista = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'rate', j));
					if (preciolista == 0){
						preciolista = nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j));
					}
					if (ComercioExterior == 'F'){
						ConceptoEx         += '<fx:PrecioLista>' + preciolista.toFixed(6) + '</fx:PrecioLista>'; 					
						ConceptoEx         += '<fx:ImporteLista>' + nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j)).toFixed(6) + '</fx:ImporteLista>';
					}
					else{
						ConceptoEx         += '<fx:PrecioLista>' + preciolista.toFixed(2) + '</fx:PrecioLista>'; 					
						ConceptoEx         += '<fx:ImporteLista>' + nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j)).toFixed(2) + '</fx:ImporteLista>';
					}
				
					subtotalbruto   = nsoParseFloatOrZero(subtotalbruto) +nsoParseFloatOrZero(invoice.getLineItemValue('item', 'amount', j));
				}
																	
					
				var basetax = nsoParseFloatOrZero(wamount).toFixed(2);
				if(basetax > 0){
					var TasaOCuota = nsoParseFloatOrZero(wtaxrate/100);
					var taxline    = nsoParseFloatOrZero(basetax*TasaOCuota).toFixed(2);
					if(taxcode != taxexento){
						retvalTaxSAT += '<fx:Traslado' +  ' Base="' + nsoParseFloatOrZero(wamount).toFixed(6) +'"' + ' Impuesto="002"' +  ' TipoFactor="Tasa"' +  ' TasaOCuota="' + nsoParseFloatOrZero(TasaOCuota).toFixed(6) +'"'  +  ' Importe="' +  nsoParseFloatOrZero(taxline).toFixed(2) +'"' + '/>';
						if(wtaxrate == '16' || wtaxrate == '16.00' || wtaxrate == '16.0'){
							newtax16 = nsoParseFloatOrZero(newtax16) + nsoParseFloatOrZero(taxline);
						}
					}
					if(taxcode == taxexento){
						retvalTaxSAT += '<fx:Traslado' +  ' Base="' + nsoParseFloatOrZero(wamount).toFixed(6) +'"' + ' Impuesto="002"' +  ' TipoFactor="Exento"' + '/>';
					}
				}
				
				
					
				if (descuento > 0){
					if (ComercioExterior == 'F'){
						retval += '<fx:Descuento>' + nsoParseFloatOrZero(descuento).toFixed(4) + '</fx:Descuento>';	
					}else{
						retval += '<fx:Descuento>' + nsoParseFloatOrZero(descuento).toFixed(2) + '</fx:Descuento>';	
					}
				}
				if (retvalTaxSAT != ''){
					retval +=  '<fx:ImpuestosSAT>';
					retval +=  '<fx:Traslados>';
					retval +=  retvalTaxSAT;
					retval +=  '</fx:Traslados>';
					retval +=  '</fx:ImpuestosSAT>';
				}

				if (yserial != null && yserial != ''){	
					ConceptoEx += '<fx:NumeroDeSerie>' + isNull(yserial) + '</fx:NumeroDeSerie>';		
				}

				if (Opciones != ''){
					retval += '<fx:Opciones>'; 
					retval += Opciones;
					retval += '</fx:Opciones>'; 
				}
					
				if (ConceptoEx != ''){
					retval += '<fx:ConceptoEx>';
					retval += ConceptoEx;
					retval += '</fx:ConceptoEx>';
				}
				retval += '</fx:Concepto>';
					
			}//end of if (witemtype == 'inventoryitem' || witemtype == 'assemblyitem' || ..........
		
		} // end of if (bandera = false)
        
		//-----> si es F es que solo salga el grupo y su total
		if (xxtype == 'Group' && printitems == 'F'){
			bandera = true;		
		}
		if (witemtype == 'EndGroup' || xxtype == 'EndGroup'){	
			bandera = false;	
			ratelinealdiscountingroup =  '';
		}			
		if (bandera == true){	
			var amountgrup = parseFloat(nlGetAmountGroup(invoice, bandera));		
		}
		
        j++;

    } //end while

    var camposhipping = parseFloat(invoice.getFieldValue('altshippingcost'));
	camposhipping = isNull(camposhipping, 0).toFixed(2);
	
	var taxrateshipping1 = isNull(nlGetGlobalTaxRate(invoice));
	taxrateshipping = taxrateshipping1/100;
	var montotaxshipping = camposhipping*taxrateshipping;
	montotaxshipping = montotaxshipping.toFixed(2);
	
 	if (camposhipping > 0 )
 	{
		var retvalTaxSAT = '';
		retval += '<fx:Concepto>'; 
		retval += '<fx:Cantidad>1</fx:Cantidad>'; 													
		retval += '<fx:ClaveUnidad>E48</fx:ClaveUnidad>';
		retval += '<fx:ClaveProdServ>01010101</fx:ClaveProdServ>';
		retval += '<fx:Descripcion>Maniobras</fx:Descripcion>'; 
		retval += '<fx:ValorUnitario>' + camposhipping + '</fx:ValorUnitario>';
		retval += '<fx:Importe>' + camposhipping + '</fx:Importe>'; 
		subtotal += nsoParseFloatOrZero(camposhipping);
		subtotalbruto   = nsoParseFloatOrZero(subtotalbruto) + nsoParseFloatOrZero(camposhipping);
		
		if(camposhipping > 0){
			var TasaOCuota = nsoParseFloatOrZero(taxrateshipping1/100);
			var taxline    = nsoParseFloatOrZero(montotaxshipping).toFixed(2)
			retvalTaxSAT += '<fx:Traslado' +  ' Base="' + nsoParseFloatOrZero(camposhipping).toFixed(2) +'"' + ' Impuesto="002"' +  ' TipoFactor="Tasa"' +  ' TasaOCuota="' + nsoParseFloatOrZero(TasaOCuota).toFixed(6) +'"'  +  ' Importe="' + nsoParseFloatOrZero(montotaxshipping).toFixed(2) +'"' + '/>';
			if(taxrateshipping1 == '16' || taxrateshipping1 == '16.00' || taxrateshipping1 == '16.0'){
				newtax16 = nsoParseFloatOrZero(newtax16) + nsoParseFloatOrZero(taxline);
			}
		}
		if (retvalTaxSAT != ''){
			retval +=  '<fx:ImpuestosSAT>';
			retval +=  '<fx:Traslados>';
			retval +=  retvalTaxSAT;
			retval +=  '</fx:Traslados>';
			retval +=  '</fx:ImpuestosSAT>';
		}
		retval += '</fx:Concepto>';
	}
	
	this.newtax16      = newtax16;
	this.newRetax16    = newRetax16;
	this.subtotal      = subtotal;
	this.subtotalbruto = subtotalbruto;
	this.descuento     = descuentototal;
	retval = retval.replace(/&/g, "&amp;");
	//retval = retval.replace(/"/g, "&quot;");
    retval += '</fx:Conceptos>';
	
    return retval;
}



//-------------------------------> Inicia trama totales    <--------------------------------// 
function Totales(invoice, customer, params, setupcfdi, ComercioExterior, anticipo) 
{
	
    var wglobaldiscount = Math.abs(parseFloat(isNull2(invoice.getFieldValue('discounttotal'), 0)));	
	var tipodecambio    = Math.abs(parseFloat(isNull2(invoice.getFieldValue('exchangerate')))).toFixed(6);
	var subtotal        = Math.abs(parseFloat(invoice.getFieldValue('subtotal'))); 
	var wdiscountrate   = isNull((wglobaldiscount*100)/subtotal);
	wdiscountrate       = isNaN(wdiscountrate) ? 0 : parseFloat(wdiscountrate);
	var taxexento       = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'));
	var cartaporte      = invoice.getFieldValue('custbody_cfdi_carta_porte')||'F';
	if (taxexento == null || taxexento == ''){taxexento = 0}
	
	if (wdiscountrate != null && wdiscountrate != ''){
		wdiscountrate = wdiscountrate.toFixed(2);
	}
	var moneda          = invoice.getFieldValue('currencysymbol');
	var date            = new Date();
	date                = nlapiAddDays(date, -1);
	date                = nlapiDateToString(date)
	var rateexchange    = nlapiExchangeRate('USD', 'MXN', date); 
	if (ComercioExterior == "T" || cartaporte == 'T'){tipodecambio = nsoParseFloatOrZero(rateexchange);}
	if (moneda != 'MXN' && invoice.getFieldValue('exchangerate') == 1.00){
		tipodecambio    = parseFloat(nlapiExchangeRate(moneda, 'MXN', date)).toFixed(2);	
	}
	if(moneda == 'MXN'){
		tipodecambio = '1';
	}									 
	var montodescuento = isNull((subtotal*wdiscountrate)/100);	
	var newamount      = 0;
	var subtotalbruto  = 0;
	var totaldescuentoporlinea = 0;
	var poswamount     = 0; 
	var taxrate	       = isNull(nlGetGlobalTaxRate(invoice));
	var taxtotal       = parseFloat(invoice.getFieldValue('taxtotal'));	
	var taxamount      = 0;
	var taxceroamount  = 0;
	var tasacompuesta  = 0;
	var retval           = '';
	var retvalTax        = '';
	var retvalTaxSAT     = '';
	var retvalTaxRet     = '';
	var retvalTaxRetSAT  = '';
	var retvalTaxIEPS    = '';
	var retvalTaxIEPSSAT = '';
	var taxtotalRet      = '';
	var newtaxtotal      = 0;
	var newTotal         = 0;
	if(taxtotal != 0){
		taxamount = parseFloat((taxtotal*100)/taxrate);
	}
	var taxexento        = isNull(setupcfdi.getFieldValue('custrecord_cfdi_taxexento'))||'';
	var taxcodeship = '';
	var amountship  = 0;
	for (var s=1 ; invoice != null && s <= invoice.getLineItemCount('shipgroup'); s++){
		var handlingtaxcode = (invoice.getLineItemValue('shipgroup','handlingtaxcode', s));
		var handlingtaxrate = parseFloat(invoice.getLineItemValue('shipgroup','handlingtaxrate', s));
		var handlingtaxamt  = parseFloat(invoice.getLineItemValue('shipgroup','handlingrate', s));
		var shippingtaxcode = (invoice.getLineItemValue('shipgroup','shippingtaxcode', s));
		var shippingtaxrate = parseFloat(invoice.getLineItemValue('shipgroup', 'shippingtaxrate', s));
		var shippingtaxamt  = parseFloat(invoice.getLineItemValue('shipgroup','shippingrate', s));

		if(handlingtaxcode != taxexento){
			var taxcodeship = handlingtaxcode;
			var tax     = handlingtaxrate;
		}
		else{
			var taxcodeship = shippingtaxcode;
			var tax     = shippingtaxrate;
		}
		amountship = nsoParseFloatOrZero(handlingtaxamt)+nsoParseFloatOrZero(shippingtaxamt);
	}

	var taxgroup    = AgrupaIVA(invoice, taxexento);
	var retvalTax = '';
	for (h in taxgroup){				
		var wxamount  = 0;
		var wtaxamt   = 0;
		for (k in taxgroup[h]){
			var wtax     = taxgroup[h][k].tax; 
			var wtaxcode = taxgroup[h][k].taxcode;
			var wxamount = wxamount + taxgroup[h][k].amount; 
			var wtaxamt  = wtaxamt  + taxgroup[h][k].taxamt;  
			//nlapiLogExecution('DEBUG','wtaxamt: ' + wtaxamt);
		}
		
		if (wdiscountrate > 0){
			if(wtaxcode == taxcodeship && amountship >0 ){wxamount = nsoParseFloatOrZero(wxamount)-nsoParseFloatOrZero(amountship);}
			wxamount = wxamount*(1-(wdiscountrate/100));
			wtaxamt  = wtaxamt*(1-(wdiscountrate/100));
			if(wtaxcode == taxcodeship && amountship >0 ){wxamount = nsoParseFloatOrZero(wxamount)-nsoParseFloatOrZero(amountship);}
		}
		if (wtaxcode != taxexento){
			var montotax = nsoParseFloatOrZero(wtaxamt).toFixed(2)
			if(wtax == '16'|| wtax == '16.00' || wtax == '16.0'){
				montotax = nsoParseFloatOrZero(this.newtax16)
			}
			newtaxtotal += nsoParseFloatOrZero(montotax)
			var TasaOCuota = nsoParseFloatOrZero(wtax/100).toFixed(6);
			retvalTaxSAT += '<fx:Traslado' + ' Impuesto="002"' +  ' TipoFactor="Tasa"' +  ' TasaOCuota="' + TasaOCuota +'"'  +  ' Importe="' + nsoParseFloatOrZero(montotax).toFixed(2) +'"' + '/>';
		}
	}
	
	var ivaretenido = AgrupaRetencion();
	var retvalTaxRet = '';
	if (ivaretenido){
		for (h in ivaretenido){
			var wxamount  = 0;
			
			for (k in ivaretenido[h]){
				var wrate     = ivaretenido[h][k].rate; 
				var wtype     = ivaretenido[h][k].tipo; 
				var wxamount  = wxamount + ivaretenido[h][k].monto; 
			}
	
			var wbase = nsoParseFloatOrZero((wxamount*100)/wrate).toFixed(2);
			var TasaOCuota = nsoParseFloatOrZero(wrate/100).toFixed(6);
			nsoParseFloatOrZero(wxamount).toFixed(2);
			taxtotalRet += nsoParseFloatOrZero(wxamount).toFixed(2)
			if(wrate == '16'|| wrate == '16.00' || wrate == '16.0'){
				taxtotalRet = nsoParseFloatOrZero(this.newRetax16)
			}
			retvalTaxRet += '<fx:Retencion' + ' Impuesto="002"' +  ' TipoFactor="Tasa"' +  ' TasaOCuota="' + TasaOCuota +'"'  +  ' Importe="' + nsoParseFloatOrZero(taxtotalRet).toFixed(2) +'"' + '/>';
	
		}
	}// end of if (ivaretenido)

	if (retvalTaxRet != '' || retvalTaxSAT != ''){
		retval += '<fx:ImpuestosSAT';
		if(retvalTaxRet != ''){
			retval += ' TotalImpuestosRetenidos="'   + nsoParseFloatOrZero(taxtotalRet).toFixed(2) + '"';
		}
		if(retvalTaxSAT != ''){
			retval += ' TotalImpuestosTrasladados="' + nsoParseFloatOrZero(newtaxtotal).toFixed(2) + '"';
		}
		retval +='>';
		if (taxtotalRet > 0){
			retval += '<fx:Retenciones>';
			retval += retvalTaxRetSAT;
			retval += '</fx:Retenciones>';
		}
		if(retvalTaxSAT != ''){
			retval += '<fx:Traslados>';
			retval += retvalTaxSAT;
			retval += '</fx:Traslados>';
		}
		retval += '</fx:ImpuestosSAT>';
	}

	retval += '<fx:Totales>';	                                                   
	retval += '<fx:Moneda>' + (invoice.getFieldValue('currencysymbol')) + '</fx:Moneda>'; 												   
	//retval += '<fx:TipoDeCambioVenta>' + tipodecambio + '</fx:TipoDeCambioVenta>';	
	retval += '<fx:SubTotalBruto>' + parseFloat(this.subtotalbruto).toFixed(2) + '</fx:SubTotalBruto>';
	retval += '<fx:SubTotal>' + parseFloat(this.subtotal).toFixed(2) + '</fx:SubTotal>';
		
	var descuentototal	= 	Math.abs(nsoParseFloatOrZero(this.descuento)).toFixed(2)
	nlapiLogExecution('DEBUG','descuentototal: ' + descuentototal);
	
		
	if (descuentototal > 0){
		retval += '<fx:Descuento>' +  nsoParseFloatOrZero(descuentototal).toFixed(2) +'</fx:Descuento>';
	}
	var newtotal = nsoParseFloatOrZero(this.subtotal)-  nsoParseFloatOrZero(descuentototal) + nsoParseFloatOrZero(newtaxtotal) - nsoParseFloatOrZero(taxtotalRet)
	newtaxtotal  = nsoParseFloatOrZero(newtaxtotal) - nsoParseFloatOrZero(taxtotalRet) 
	var wtype    = invoice.getRecordType();
	var wId      = invoice.getId()
		
	if (nsoParseFloatOrZero(newtaxtotal).toFixed(2) != nsoParseFloatOrZero(invoice.getFieldValue('taxtotal'))	){
		var diftax = nsoParseFloatOrZero(taxtotal-newtaxtotal).toFixed(2);
		nlapiSubmitField(wtype, wId, 'custbody_dif_tax', diftax)
	}
	var tot    = nsoParseFloatOrZero(invoice.getFieldValue('total'));
	var diftot = nsoParseFloatOrZero(tot - newtotal).toFixed(2);
	diftot     = Math.abs(diftot)
	if( diftot > dif_centavos ){
		retval = '';
		nlapiLogExecution('DEBUG','retval diferencia total: ' + diftot);	
		nlapiSubmitField(wtype, wId, 'custbody_dif_total', diftot)
	}
	else{
		var Total           = nsoParseFloatOrZero(newtotal).toFixed(2);
		var currency_symbol = invoice.getFieldValue('currencysymbol');
		nlapiLogExecution('DEBUG','currency_symbol: ' + currency_symbol);	
		var objConverter    = new NSObjConverterAmountToWords();
		var words           = objConverter.toWords(Total, "spanish", currency_symbol);
		
		retval += '<fx:Total>' + Total + '</fx:Total>';		
		retval += '<fx:TotalEnLetra>' + words + '</fx:TotalEnLetra>';			
		var metododepago    = invoice.getFieldValue('custbody_cfdi_metpago_sat');
		var claveMetodoPago = '';
		//nlapiLogExecution('DEBUG','metododepago: ' + metododepago);	
		if (metododepago != null && metododepago != ''){
			claveMetodoPago = MetPago(metododepago);	
		}	
		retval += '<fx:FormaDePago>' + claveMetodoPago + '</fx:FormaDePago>';
		retval += '</fx:Totales>';
	}

	return retval;
}




//================================== Comprobante Ex =======================================


function ComprobanteEx(invoice, customer, params, setupcfdi, ComercioExterior, anticipo)
{
	var retval          = '';
	var metododepago    = invoice.getFieldValue('custbody_cfdi_metpago_sat');
	var terms           = invoice.getFieldText('terms');
	var leyendasfisc    = invoice.getFieldValue('custbody_cfdi_leyfiscales');
	var memo            = ''//invoice.getFieldValue('memo');
	var testing         = setupcfdi.getFieldValue('custrecord_cfdi_testing');
	var custid          = invoice.getFieldValue("entity");
	var tipodedocumento = getTipoDocumento(invoice.getRecordType());
	if (leyendasfisc != null && leyendasfisc != ''){
	 	retval += '<fx:Complementos>';
		 	retval += '<fx:LeyendasFiscales>';
			 	retval += '<fx:Leyenda>';
				 	retval += '<fx:TextoLeyenda>' + leyendasfisc + '</fx:TextoLeyenda>';
				retval += '</fx:Leyenda>';
			retval += '</fx:LeyendasFiscales>';
		retval += '</fx:Complementos>';
	}
	
	retval += '<fx:ComprobanteEx>';
	retval += '<fx:DatosDeNegocio>';
	if (testing == 'T')
	{retval += '<fx:Sucursal>' + setupcfdi.getFieldValue('custrecord_cfdi_sucursal_testing') + '</fx:Sucursal>'; }
	else
	{retval += '<fx:Sucursal>' + params.getFieldValue('custrecord_cfdi_sucursal_mysuite') + '</fx:Sucursal>';}
	retval += '</fx:DatosDeNegocio>';
	var formadepago     = isNull(invoice.getFieldText('custbody_cfdi_formadepago'))||'';
	if (formadepago != ''){
		var arrayPago = new Array();
		var arrayPago = formadepago.split('-');
		formadepago = arrayPago[0];
	}
	if(tipodedocumento == 'NOTA_DE_CREDITO' && cambioValoresNC == 'T'){
		formadepago = params.getFieldValue('custrecord_cfdi_metodopago_nc')||'PUE';
	}
	if (anticipo == 'T'){
		formadepago = params.getFieldValue('custrecord_cfdi_formapago_anticipo')||'PUE';
	}
	
	retval += '<fx:TerminosDePago>';
	retval += '<fx:MetodoDePago>' + formadepago + '</fx:MetodoDePago>';
	retval += '</fx:TerminosDePago>';
		
	
		
	nlapiLogExecution('DEBUG','retval: ' + retval);
	if((memo != null && memo != ''))
	{
		retval += '<fx:TextosDePie>';
			retval += '<fx:Texto>'+ isNull(memo) + '</fx:Texto>';
		retval += '</fx:TextosDePie>';
	}

	retval += '</fx:ComprobanteEx>';
	retval += '</fx:FactDocMX>'; 
	retval = retval.replace(/&/g, "&amp;");
	retval = retval.replace(/"/g, "&quot;");
	return retval;

}
 
function SearchGift(invoice){
	var retval = '';
	var giftcode = '';
	for (var j=1 ; j <= invoice.getLineItemCount('giftcertredemption'); j++){
		var giftcode =  invoice.getLineItemValue('giftcertredemption', 'authcode_display', j); 
		if(giftcode != null && giftcode != ''){break;}
	}
	
	if(giftcode != null && giftcode != ''){

		var filters = new Array();
		filters.push(new nlobjSearchFilter('gccode', null, 'is', giftcode));
		   
		var columns = new Array();
		columns.push(new nlobjSearchColumn('item'));

		var searchresult  = nlapiSearchRecord('giftcertificate', null, filters, columns);
		for(var i = 0 ; searchresult != null && i < searchresult.length; i++){
			var iditem           = searchresult[i].getValue('item');
			if(iditem != null && iditem != ''){
				retval = iditem;
				break;
			}
		}
	}

	return retval;
}

function MetPago(idMetdPago){
	var retval = '';
	if(idMetdPago){
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cfdi_payment_met_nat', null, 'anyof', idMetdPago));
		   
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cfdi_payment_met_sat'));

		var searchresult  = nlapiSearchRecord('customrecord_cfdi_metododepago', null, filters, columns);
		nlapiLogExecution('DEBUG','searchresult: ' + searchresult);
		for(var i = 0 ; searchresult != null && i < searchresult.length; i++){
			var idMet          = searchresult[i].getText('custrecord_cfdi_payment_met_sat');
			if(idMet != null && idMet != ''){
				var arrayMP = new Array();
				arrayMP = idMet.split('-');
				if(arrayMP.length > 1){
					retval = arrayMP[0];
				}
			}
		}
	}
	return retval;
}
  		
function MetPagoAlt(idMetdPago)
{
	var retval = '';
	if(idMetdPago){
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_cfdi_payment_met_nat', null, 'anyof', idMetdPago));
		   
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cfdi_payment_met_sat'));

		var searchresult  = nlapiSearchRecord('customrecord_cfdi_metododepago', null, filters, columns);

		for(var i = 0 ; searchresult != null && i < searchresult.length; i++){
			var idMet          = searchresult[i].getText('custrecord_cfdi_payment_met_sat');
			if(idMet != null && idMet != ''){
				var arrayMP = new Array();
				arrayMP = idMet.split('-');
				if(arrayMP.length > 1){
					retval += arrayMP[0]+',';
				}
			}
		}
	}
	if(retval != null && retval != ''){
	retval = rtrimcoma(retval);
	}
	return retval;
}


function UnidadMedida()
{
	var searchresult = '';
	
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_cfdi_clav_unidad'));
	columns.push(new nlobjSearchColumn('custrecord_cfdi_nat_units'));
	searchresult  = nlapiSearchRecord('customrecord_cfdi_clave_unidad', null, null, columns);

	return searchresult;
}




//=============================== Fin Comprobante Ex ========================================

function TotalRetencionesIVA(invoice)
{
	var amount = 0;
	var retval = 0;
	
	for (var j=1 ; j <= invoice.getLineItemCount('item'); j++)
	{
		var subitemof = invoice.getLineItemValue('item', 'custcol_cfdi_retencion', j);
		
		if (subitemof == idretIVA)
		{
			amount = Math.abs(parseFloat(invoice.getLineItemValue('item', 'amount', j)));
			retval += amount
		}
	}
	return retval;
}

function TotalRetencionesISR(invoice)
{
	var amount = 0;
	var retval = 0;
	
	for (var j=1 ; j <= invoice.getLineItemCount('item'); j++)
	{
		var subitemof = invoice.getLineItemValue('item', 'custcol_cfdi_retencion', j);
		
		if (subitemof == idretISR)
		{
			amount = Math.abs(parseFloat(invoice.getLineItemValue('item', 'amount', j)));
			retval += amount
		}
	}
	return retval;
}

function RetencionesInLine(line, record)
{
	var arrayretencion = '';
	
	for (var j=1; j <= record.getLineItemCount('item'); j++)
	{
		var discline = parseFloat(record.getLineItemValue('item', 'discline', j));
		var subitemof = (record.getLineItemValue('item', 'custcol_cfdi_retencion', j));
		
		if ((discline == line) && (subitemof == idretIVA))
		{
			arrayretencion = new Array();
		    var amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			var rate = parseFloat(record.getLineItemValue('item', 'rate', j));
			
			arrayretencion[0] = amount;
			arrayretencion[1] = rate;
			arrayretencion[2] = 'IVA';
		}
		if ((discline == line) && (subitemof == idretISR))
		{

			 arrayretencion = new Array();
		     var amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			 var rate = parseFloat(record.getLineItemValue('item', 'rate', j));
			 
			 arrayretencion[0] = amount;
			 arrayretencion[1] = rate;
			 arrayretencion[2] = 'ISR';
		}

	}

	return arrayretencion;
}

function AgrupaRetencion()
{
	var arrTax = new Array();
	
	for(var i = 0; i < this.TasaRetencion.length; i++)
	{		
		var rate    = this.TasaRetencion[i].rate;
		var indrate = rate * 100;
		var monto   = this.TasaRetencion[i].monto;
		var tipo    = this.TasaRetencion[i].tipo		
		if (indrate != null && indrate != "" )
		{	
			var index = null;
				
			try
			{
				index = arrTax[indrate].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[indrate] = new Array();
			}
			
			index = arrTax[indrate].length;								
			arrTax[indrate][index] = new RetA(rate, monto, tipo);
		}			
	}
	
	return arrTax;
}




function RetA(rate, monto, tipo)
{
	this.rate = rate;	
	this.monto = nsoParseFloatOrZero(this.monto) + nsoParseFloatOrZero(monto);	
	this.tipo = tipo;	
}


function TasaRetencion(rate, monto, tipo)
{
	this.rate  = rate;	
	this.monto = monto;	
	this.tipo  = tipo;	
}




function AgrupaDescTipoA()
{
	var arrTax = new Array();
	
	for(var i = 0; i < this.tasades.length; i++)
	{		
		var rate    = this.tasades[i].rate;
		var indrate = rate * 100;
		var base    = this.tasades[i].base;
		var monto   = this.tasades[i].monto;
				
		if (indrate != null && indrate != "" )
		{	
			var index = null;
				
			try
			{
				index = arrTax[indrate].length;
			}
			catch(err)
			{			
			}
		
			if (index == null)
			{
				arrTax[indrate] = new Array();
			}
			
			index = arrTax[indrate].length;								
			arrTax[indrate][index] = new DesA(rate, base, monto);
		}			
	}
	
	return arrTax;
}


function DesA(rate, base, monto)
{
	this.rate = rate;	
	this.monto = nsoParseFloatOrZero(this.monto) + nsoParseFloatOrZero(monto);	
	this.base  = nsoParseFloatOrZero(this.base) + nsoParseFloatOrZero(base);	
}

function TasaDescuentos(rate, base, monto)
{
	this.base = base;
	this.rate = rate;	
	this.monto = monto;	
}


function AgrupaIVA(record, taxexento)
{
	var arrTax = new Array();
	
			
	
	for(var i = 1; record != null && i <= record.getLineItemCount('item'); i++)
	{
		var tax       = parseFloat(record.getLineItemValue('item', 'taxrate1', i)); 	
		var amount    = parseFloat(record.getLineItemValue('item', 'amount', i)); 	
		var taxcode   = record.getLineItemValue('item', 'taxcode', i); 	
		var typeItem  = record.getLineItemValue('item', 'itemtype', i); 			
		var taxamt    = parseFloat(record.getLineItemValue('item', 'tax1amt', i));		
		nlapiLogExecution('DEBUG','taxamt: ' + taxamt);							
		if (taxcode != null && taxcode != "" && typeItem != 'EndGroup' && typeItem != 'Group' && typeItem != 'Description' && taxcode != taxexento){						
			var index  = null;	
			try{
				index = arrTax[taxcode].length;
			}
			catch(err){	}
		
			if (index == null){
				arrTax[taxcode] = new Array();
			}
			
			index = arrTax[taxcode].length;								
			arrTax[taxcode][index] = new Tax(tax, taxcode, amount, taxamt);
		}		
	}

	for (var s=1 ; record != null && s <= record.getLineItemCount('shipgroup'); s++){
		var handlingtaxcode = (record.getLineItemValue('shipgroup','handlingtaxcode', s));
		var handlingtaxrate = parseFloat(record.getLineItemValue('shipgroup','handlingtaxrate', s));
		var handlingtaxamt  = parseFloat(record.getLineItemValue('shipgroup','handlingrate', s));
		var shippingtaxcode = (record.getLineItemValue('shipgroup','shippingtaxcode', s));
		var shippingtaxrate = parseFloat(record.getLineItemValue('shipgroup', 'shippingtaxrate', s));
		var shippingtaxamt  = parseFloat(record.getLineItemValue('shipgroup','shippingrate', s));

		if(handlingtaxcode != taxexento){
			var taxcode = handlingtaxcode;
			var tax     = handlingtaxrate;
		}
		else{
			var taxcode = shippingtaxcode;
			var tax     = shippingtaxrate;
		}

		var amount          = nsoParseFloatOrZero(handlingtaxamt)+nsoParseFloatOrZero(shippingtaxamt);

		if (taxcode != taxexento && taxcode != null && taxcode != "" && amount > 0){
			var index1 = null;	
			try{
				index1 = arrTax[taxcode].length;
			}
			catch(err){	}
			if (index1 == null){
				arrTax[taxcode] = new Array();
			}
			
			index1 = arrTax[taxcode].length;								
			arrTax[taxcode][index1] = new Tax(tax, taxcode, amount, '');
		}
    }
	
	return arrTax;
}

function Tax(tax, taxcode, amount, taxamt)
{
	this.tax = tax;
	this.taxcode = taxcode;
	this.amount = amount;	
	this.taxamt = taxamt;	
}

function nsoParseFloatOrZero(f)
{
   var r=parseFloat(f);
   return isNaN(r) ? 0 : r;
}

function rtrimcoma(s) 
{
	
	var retval = "";
	
    // Quita los espacios en blanco del final de la cadena
	
	if(s != null && s != "")
	{
		var j = 0;
	
		// Busca el �ltimo caracter <> de un espacio
		for (var i = s.length - 1; i > -1; i--)
			if (s.substring(i, i + 1) != ',') {
			j = i;
			break;
		}
		
		retval = s.substring(0, j + 1);
	}
	
	return retval;
}




function nlGetGlobalTaxRate(invoice)
{
	var retval = 0;
	
	for(var i = 1; i <= invoice.getLineItemCount("item"); i++)
	{
		var taxrate = nsoParseFloatOrZero(invoice.getLineItemValue("item", "taxrate1", i));
		//esta funcion convierte el string en numero, en este caso es de 16.0% a 16
		if(taxrate >= retval)
		{
			retval = taxrate;
		}
	}
	
	retval = retval.toFixed(2);
	//esta funcion "tofixed(2)" sirve para ponerle los 2 decimales al numero 
	return retval;
}




function isNull(value) {
    return (value == null) ? '' : value;
}

function isNull2(value, replaceby) {
    return (value == null) ? replaceby : value;
}

function LTrim(s) {
	
	var retval = "";
	
	if(s != null && s != "")
	{
		// Devuelve una cadena sin los espacios del principio
		var i = 0;
		var j = 0;
	
		// Busca el primer caracter <> de un espacio
		for (i = 0; i <= s.length - 1; i++)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval =  s.substring(j, s.length);
	}
	
	return retval;
}
function RTrim(s) {
	
	var retval = "";
	
    // Quita los espacios en blanco del final de la cadena
	
	if(s != null && s != "")
	{
		var j = 0;
	
		// Busca el �ltimo caracter <> de un espacio
		for (var i = s.length - 1; i > -1; i--)
			if (s.substring(i, i + 1) != ' ' && s.substring(i, i + 1) != '') {
			j = i;
			break;
		}
		
		retval = s.substring(0, j + 1);
	}
	
	return retval;
}
function Trim(s) {
    // Quita los espacios del principio y del final
    return LTrim(RTrim(s));
}


function getTipoDocumento(recordtype) {
    var retval = '';

    switch (recordtype) {
        
        case 'creditmemo':
            retval = 'NOTA_DE_CREDITO'
            break;
		case  'invoice':
			retval = 'FACTURA';
			break;
    }

    return retval;
}

function setTipoComprobante(recordtype) {
    var retval = '';

    switch (recordtype) {
        case 'invoice':
            retval = 'ingreso';
            break;
        case 'creditmemo':
            retval = 'egreso'
            break;
    }

    return retval;
}



function replace(texto, s1, s2) {
	try{
    	return texto.split(s1).join(s2);
	}
	catch(err)
	{
		
	}
}
	
function fmtInt(number, digits)
{
	var wnumber = number.toString();
	
	if (wnumber.length < digits)
	{
		for (var i = 1; i <= digits - number.toString().length; i++)
		{
		wnumber = "0" + wnumber.toString();
		}
	}

return wnumber;
}


function getDiscountRate(value) {
    if (value == null || value == '') return 0;
    var arr_value = value.split('%');
    return arr_value[0];
}


//--------------------------------------------------------
// ------------------- String functions ------------------
//--------------------------------------------------------
function InStr(n, s1, s2) {
	// Devuelve la posici�n de la primera ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=InStr.arguments.length;
	
	if(numargs<3)
		return n.indexOf(s1)+1;
	else
		return s1.indexOf(s2, n)+1;
}
function RInStr(n, s1, s2){
	// Devuelve la posici�n de la �ltima ocurrencia de s2 en s1
	// Si se especifica n, se empezar� a comprobar desde esa posici�n
	// Sino se especifica, los dos par�metros ser�n las cadenas
	var numargs=RInStr.arguments.length;
	
	if(numargs<3)
		return n.lastIndexOf(s1)+1;
	else
		return s1.lastIndexOf(s2, n)+1;
}


function ObtSerialNumbersOrPedimento(arrseries, p_numparte)
{
	var xseries = "";
	nlapiLogExecution('DEBUG', 'arrseries.length:   ', arrseries.length);
	for(var j = 0; j < arrseries.length; j++)
	{	
		if(arrseries[j] != null && arrseries[j] != "")
		{
			var fin       = arrseries[j].length;
			var xindex    = arrseries[j].indexOf(")");
			var xnumparte = arrseries[j].substring(0,xindex);	
			xnumparte	  = xnumparte.replace(/\s/g, "");
			
			nlapiLogExecution('DEBUG', 'xnumparte.length:   ', xnumparte.length);
			nlapiLogExecution('DEBUG', 'xnumparte:   ', xnumparte);
			nlapiLogExecution('DEBUG', 'p_numparte:   ', p_numparte);
			
			if(p_numparte == xnumparte)
			{
				xseries   = arrseries[j].substring(xindex + 1,fin);
				xseries   = xseries.replace(/\S\s\S/g, ',');
				xseries   = xseries.replace(/\s/g, "");
				nlapiLogExecution('DEBUG', 'xseries:   ', xseries);
				break;
			}
		}
	}
	return xseries;
}

function ArraySerialNumber(invoice)
{	
	var arrseries = new Array();
	
	for(var i = 1; i <= invoice.getLineItemCount('item'); i++)
	{
		var witem = invoice.getLineItemValue('item', 'item', i);
		if(witem == 23490) //Series
		{
			var series = invoice.getLineItemValue('item','description',i);
			nlapiLogExecution('DEBUG', 'series:   ', series);
			
			if(series != null && series != "")
			{
			   arrseries = series.split('(');			   
			   break;
			} 
		}
	}
	
	return arrseries;
}



function DiscountForItem(line, record)
{
	var discarray = new Array();
	var amount    = 0;
	var amounttax = 0;
	for (var j=1 ; j <= record.getLineItemCount('item'); j++){
		var discline = nsoParseFloatOrZero(record.getLineItemValue('item', 'discline', j));
		if (discline == line){
			amount    += nsoParseFloatOrZero(record.getLineItemValue('item', 'amount', j));
			amounttax += nsoParseFloatOrZero(record.getLineItemValue('item', 'tax1amt', j));
		}
	}
	if (amount != 0){
		discarray = [amount, amounttax];
	}else{
		discarray = '';
	}
	
	return discarray;
}



function AgroupSerials(xserials)
{
	var numline = 0;
	var strserails = "";
	var separador = ",";
	for(var i = 0; xserials != null && i < xserials.length; i++ )
	{
		numline += 1;
		if(i == 0)
		{  
			strserails = xserials[i]; 
		}
		else
		{
			if(numline == 5)
			{
			  separador = "\n";
			  numline = 0; 
			}
			else
			{
			  separador = ",";
			}
			strserails = strserails + separador + xserials[i];
		}
	
	}

	return strserails;

}

function nlGetAmountGroup(invoice, bandera)
{
	var retval = 0;
	
	for(var i = 1; i <= invoice.getLineItemCount("item"); i++)
	{
		if (bandera == true)
		{
			var itemtype = invoice.getLineItemValue('item', 'itemtype', i);
			if (itemtype  == 'EndGroup')
			{var amountgroup = invoice.getLineItemValue('item', 'amount', i);}
		}

	}
	retval  = nsoParseFloatOrZero(amountgroup);
	return retval;
}


function IEPSItem(line, record)
{
	var amount = 0;
	
	for (var j=1 ; j <= record.getLineItemCount('item'); j++)
	{
		var itemid = parseFloat(record.getLineItemValue('item', 'item', j));
		if (itemid == '1004')
		{
			amount = parseFloat(record.getLineItemValue('item', 'amount', j));
			break;
		}
	}
	
	return amount;

}



function GetExchangeRate(currency)
{
   var exchagerate = 1;
   
   var filters = new Array();
   filters.push(new nlobjSearchFilter('internalid', null, 'anyof', currency));
   
   var columns = new Array();
   columns.push(new nlobjSearchColumn('exchangerate'));
   
   var searchresults = nlapiSearchRecord('currency', null, filters, columns);

   if (searchresults != null && searchresults.length > 0){
       exchagerate = searchresults[0].getValue('exchangerate');
   }

   return exchagerate;
}


function Catalogopaises(value)
{
   var retval = false;
   
   var filters = new Array();
   filters.push(new nlobjSearchFilter('custrecord_ce_pais', null, 'is', value));
   
   var columns = new Array();
   columns.push(new nlobjSearchColumn('custrecord_ce_agrupaciones'));
   
   var searchresults = nlapiSearchRecord('customrecord_ce_catalogo_paises', null, filters, columns);

   if (searchresults != null && searchresults.length > 0){
       var agrupacion = searchresults[0].getValue('custrecord_ce_agrupaciones');
       if (agrupacion == 'Union Europea'){
       		retval = true
       }
   }

   return retval;
}

function CatalogoAduanas()
{
	var searchresults = '';
	   
    var columns = new Array();
    columns.push(new nlobjSearchColumn('custrecord_cfdi_aduana_clave'));
	columns.push(new nlobjSearchColumn('custrecord_cfdi_aduana_name'));

   
    var searchresults = nlapiSearchRecord('customrecord_cfdi_tabla_aduanas', null, null, columns);

	return searchresults;
	
}

function RateDiscountGroup(j, invoice)
{
	var rate ='';
	
		var witemid = invoice.getLineItemValue('item', 'item', j);
		var xxtype = invoice.getLineItemValue('item', 'itemtype', j);
		//var witemtype = nlapiLookupField('item', witemid, 'recordtype');
			
		if ( xxtype == 'Group')
		{	
			for (var m=j+1 ; m <= invoice.getLineItemCount('item'); m++)
			{
				var itemid = invoice.getLineItemValue('item', 'item', m);
				var type = invoice.getLineItemValue('item', 'itemtype', m);
				if (type == 'EndGroup')
				{
					var discline = (invoice.getLineItemValue('item', 'itemtype', m+1));
					if (discline == 'Discount')
					{
						var rate = parseFloat(invoice.getLineItemValue('item', 'rate', m+1));
						break;
					}
					else
					{rate = '';
					break;}
				}
			}
		}
	return rate;
}

function formatDate1(str, fmt) {
    var format = '';
    if (str == null || str == '') return '';
    format = (fmt != null && fmt != '') ? fmt : window.dateformat;
    var d = convertStringToDate(str, format, true);
    var curr_date = d.getDate().toString();
    var curr_month = (d.getMonth() + 1).toString();
    var curr_year = d.getFullYear().toString();
    curr_date = (curr_date.length < 2) ? '0' + curr_date : curr_date;
    curr_month = (curr_month.length < 2) ? '0' + curr_month : curr_month;
    curr_year = (curr_year.length < 4) ? '0' + curr_year : curr_year;
	return curr_year + '-' + curr_month + '-' + curr_date;
}

function formatDate2(strdate)
{
	var retval = "";
	var date = nlapiStringToDate(strdate);
	retval += date.getFullYear() + '-' + fmtInt(date.getMonth()+1, 2) + '-' + fmtInt(date.getDay(), 2);
	//retval += 'T' + fmtInt(date.getHours(), 2) +':' + fmtInt(date.getMinutes(), 2) +':' + fmtInt(date.getSeconds(), 2);
	return retval;
}
	
function fmtInt(number, digits)
{
	var wnumber = number.toString();
	
	if (wnumber.length < digits)
	{
		for (var i = 1; i <= digits - number.toString().length; i++)
		{
		wnumber = "0" + wnumber.toString();
		}
	}

return wnumber;
}

function GetRelacionados(invoice){
	
	var retval = new Array();
	var count  = 0;
	for (var x=1; x<= invoice.getLineItemCount('recmachcustrecord_cfdi_tabla_padre'); x++){
		var UUID = invoice.getLineItemValue('recmachcustrecord_cfdi_tabla_padre', 'custrecord_cfdi_rel_uuid', x)||'';
		if(UUID != ''){
			retval[count]= UUID;
			count +=1;
		}
	}
	return retval;
	
}

function SearchkitItems(record){
	var searchresults = '';
	var count         = 0;
	var arrayitems    = new Array();
	for (var x=1; x<= record.getLineItemCount('item'); x++){
		var witemtype  = record.getLineItemValue('item', 'itemtype', x)||'';
		if (witemtype  == 'Kit'){
			arrayitems[count] = record.getLineItemValue('item', 'item', x);
			count +=1;
		}
	}
	
	if (arrayitems.length > 0){
	    var filters = new Array();
	    filters.push(new nlobjSearchFilter('internalid', null, 'anyof', arrayitems));
	    var columns = new Array();
	   	columns.push(new nlobjSearchColumn('memberitem'));
		columns.push(new nlobjSearchColumn('memberquantity'));
		columns.push(new nlobjSearchColumn('custitem_clave_prod_serv', 'memberItem'));
		columns.push(new nlobjSearchColumn('saleunit', 'memberItem'));
		
	    var searchresults = nlapiSearchRecord('item', null, filters, columns);
	}
	
	return searchresults;
}